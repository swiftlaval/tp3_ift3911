package database.sections;

import database.dispositions.Disposition;
import database.places.Place;

import java.util.List;

public class Famille extends SectionPaquebot {

	private String classe = "F";
	private double ratio = 0.9;

    public Famille(Disposition dispositionSieges, int nbRangees, double prix) {
        super(dispositionSieges, nbRangees, prix);
        places = dispositionSieges.construirePlaces(classe, nbRangees);
    }

	@Override
	public double getPrix() {
		return ratio * prix;
	}

	@Override
	public String getClasse() {
		return classe;
	}
}