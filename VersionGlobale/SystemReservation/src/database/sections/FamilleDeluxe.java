package database.sections;

import database.dispositions.Disposition;
import database.places.Place;

import java.util.List;

public class FamilleDeluxe extends SectionPaquebot {

	private String classe = "D";
	private double ratio = 1;

    public FamilleDeluxe(Disposition dispositionSieges, int nbRangees, double prix) {
        super(dispositionSieges, nbRangees, prix);
        places = dispositionSieges.construirePlaces(classe, nbRangees);
    }

	@Override
	public double getPrix() {
		return ratio * prix;
	}

	@Override
	public String getClasse() {
		return classe;
	}
}