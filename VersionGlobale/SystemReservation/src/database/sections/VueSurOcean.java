package database.sections;

import database.dispositions.Disposition;
import database.places.Place;

import java.util.List;

public class VueSurOcean extends SectionPaquebot {

	private String classe = "O";
	private double ratio = 0.75;

    public VueSurOcean(Disposition dispositionSieges, int nbRangees, double prix) {
        super(dispositionSieges, nbRangees, prix);
        places = dispositionSieges.construirePlaces(classe, nbRangees);
    }

	@Override
	public double getPrix() {
		return ratio * prix;
	}

	public String getClasse() {
		return classe;
	}
}