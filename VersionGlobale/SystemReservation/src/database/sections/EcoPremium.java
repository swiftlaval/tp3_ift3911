package database.sections;

import database.dispositions.Disposition;
import database.places.Place;

import java.util.List;

public class EcoPremium extends SectionAvion {

	private String classe = "P";
	private double ratio = 0.6;

    public EcoPremium(Disposition dispositionSieges, int nbRangees, double prix) {
        super(dispositionSieges, nbRangees, prix);
        places = dispositionSieges.construirePlaces(classe, nbRangees);
    }

	@Override
	public double getPrix() {
		return ratio * prix;
	}

	public String getClasse() {
		return classe;
	}
}