package database.sections;

import database.*;
import database.dispositions.*;
import java.util.*;
import database.places.*;
import patrons.Visiteur;

public abstract class Section implements IVisitable {

	protected Disposition dispositionSieges;
	protected List<Place> places;
	protected double prix;

    protected Section(Disposition dispositionSieges, int nbRangees, double prix) {
    	this.dispositionSieges = dispositionSieges;
    	this.prix = prix;
    }

    public abstract double getPrix();
    
    public abstract String getClasse();

	/**
	 * 
	 * @param v
	 */
	public void accept(Visiteur v) {
		v.visit(this);
	}

	/**
	 * 
	 * @param priorite
	 */
	public Place trouverPlace(int priorite) {
		for(Place i :this.places){
			if(i.getEmplacement()==priorite && i.estLibre()){
				return i;
			}
		}
		for(Place i :this.places){
			if(i.estLibre()){
				return i;
			}
		}
		return null;
	}

	public Disposition getDisposition() {
		return dispositionSieges;
	}

	public int getNbPlacesLibres() {
		int placesLibres = 0;
		for (Place place : places) {
			if (place.estLibre())
				placesLibres++;
		}
		return placesLibres;
	}
	
	public int getNbPlacesTotal() {
		return places.size();
	}
}