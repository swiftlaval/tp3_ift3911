package database.sections;

import database.dispositions.Disposition;
import database.places.Place;

import java.util.List;

public abstract class SectionPaquebot extends Section {

    protected SectionPaquebot(Disposition dispositionSieges, int nbRangees, double prix) {
        super(dispositionSieges, nbRangees, prix);
    }
}