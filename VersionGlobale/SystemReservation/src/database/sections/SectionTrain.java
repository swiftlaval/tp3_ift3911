package database.sections;

import database.dispositions.Disposition;
import database.places.Place;

import java.util.List;

public abstract class SectionTrain extends Section {

    protected SectionTrain(Disposition dispositionSieges, int nbRangees, double prix) {
        super(dispositionSieges, nbRangees, prix);
    }
}