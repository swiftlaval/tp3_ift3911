package database.sections;

import database.dispositions.Disposition;
import database.places.Place;

import java.util.List;

public class Economique extends SectionAvion {

	private String classe = "E";
	private double ratio = 0.5;

    public Economique(Disposition dispositionSieges, int nbRangees, double prix) {
        super(dispositionSieges, nbRangees, prix);
        places = dispositionSieges.construirePlaces(classe, nbRangees);
    }

	@Override
	public double getPrix() {
		return ratio * prix;
	}

	@Override
	public String getClasse() {
		return classe;
	}
}