package database.sections;

import database.dispositions.Disposition;
import database.places.Place;

import java.util.List;

public abstract class SectionAvion extends Section {

	protected SectionAvion(Disposition dispositionSieges, int nbRangees, double prix) {
		super(dispositionSieges, nbRangees, prix);
	}
}