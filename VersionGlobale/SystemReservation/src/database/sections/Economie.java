package database.sections;

import database.dispositions.Disposition;
import database.places.Place;

import java.util.List;

public class Economie extends SectionTrain {

	private String classe = "F";
	private double ratio = 0.5;

    public Economie(Disposition dispositionSieges, int nbRangees, double prix) {
        super(dispositionSieges, nbRangees, prix);
        places = dispositionSieges.construirePlaces(classe, nbRangees);
    }

	@Override
	public double getPrix() {
		return ratio * prix;
	}

	@Override
	public String getClasse() {
		return classe;
	}
}