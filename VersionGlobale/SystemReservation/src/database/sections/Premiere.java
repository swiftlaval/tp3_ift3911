package database.sections;

import database.dispositions.Disposition;
import database.places.Place;

import java.util.List;

public class Premiere extends SectionAvion {

	private String classe = "F";
	private double ratio = 1;

    public Premiere(Disposition dispositionSieges, int nbRangees, double prix) {
        super(dispositionSieges, nbRangees, prix);
        places = dispositionSieges.construirePlaces(classe, nbRangees);
    }

	@Override
	public double getPrix() {
		return ratio * prix;
	}

	public String getClasse() {
		return classe;
	}
}