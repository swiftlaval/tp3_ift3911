package database.places;

public class PlaceConfirmee implements EtatPlace {

	private Place place;
	
	public PlaceConfirmee(Place place){
		this.place = place;
	}

	public boolean libererPlace() {
		place.setEtat(place.getPlaceLibre());
		return true;
	}

	public boolean confirmerPlace() {
		System.out.println("Cette place est deja confirmee");
		return false;
	}

	public boolean reserverPlace() {
		System.out.println("Cette place est deja confirmee");
		return false;
	}

}