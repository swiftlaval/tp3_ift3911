package database.places;

public interface EtatPlace {

	boolean libererPlace();

	boolean confirmerPlace();

	boolean reserverPlace();

}