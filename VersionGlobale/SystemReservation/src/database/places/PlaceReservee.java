package database.places;

public class PlaceReservee implements EtatPlace {

	private Place place;
	
	public PlaceReservee(Place place){
		this.place = place;
	}

	public boolean libererPlace() {
		place.setEtat(place.getPlaceLibre());
		return true;
	}

	public boolean confirmerPlace() {
		place.setEtat(place.getPlaceConfirmee());
		return true;
	}

	public boolean reserverPlace() {
		System.out.println("Cette place est deja reservee");
		return false;
	}

}