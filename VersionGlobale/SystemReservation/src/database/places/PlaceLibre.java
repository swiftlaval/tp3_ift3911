package database.places;

public class PlaceLibre implements EtatPlace {

	private Place place;
	
	public PlaceLibre(Place place){
		this.place = place;
	}

	public boolean libererPlace() {
		return true;
	}

	public boolean confirmerPlace() {
		//Ne sera jamais execute, sauf s'il y a erreur dans le reste du code.
		System.out.println("Veuilliez d'abord reserver la place.");
		return false;
	}

	public boolean reserverPlace() {
		place.setEtat(place.getPlaceReservee());
		return true;
	}

}