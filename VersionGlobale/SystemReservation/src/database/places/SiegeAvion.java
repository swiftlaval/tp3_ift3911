package database.places;

public class SiegeAvion extends Place {

	private int rangee;
	private int colonne;


	public SiegeAvion() {
		super();
	}

	public int getRangee() {
		return rangee;
	}

	public void setRangee(int rangee) {
		this.rangee = rangee;
	}

	public int getColonne() {
		return colonne;
	}

	public void setColonne(int colonne) {
		this.colonne = colonne;
	}
}