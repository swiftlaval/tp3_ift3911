package database.places;

import database.sections.*;
import reservations.*;

public abstract class Place {

	private String id;
	private Section section;
	private Reservation reservation;
	private EtatPlace placeLibre;
	private EtatPlace placeConfirmee;
	private EtatPlace placeReservee;
	private EtatPlace etat;
	private int emplacement=0;

	public Place(){
		this.placeLibre = new PlaceLibre(this);
		this.placeConfirmee = new PlaceConfirmee(this);
		this.placeReservee = new PlaceReservee(this);
		this.etat = placeLibre;
	}

	public int getEmplacement() {
		return emplacement;
	}

	public void setEmplacement(int emplacement) {
		this.emplacement = emplacement;
	}

	public EtatPlace getPlaceLibre() {
		return placeLibre;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId(String id) {
		return id;
	}

	public void setPlaceLibre(EtatPlace placeLibre) {
		this.placeLibre = placeLibre;
	}


	public EtatPlace getPlaceConfirmee() {
		return placeConfirmee;
	}


	public void setPlaceConfirmee(EtatPlace placeConfirmee) {
		this.placeConfirmee = placeConfirmee;
	}


	public EtatPlace getPlaceReservee() {
		return placeReservee;
	}


	public void setPlaceReservee(EtatPlace placeReservee) {
		this.placeReservee = placeReservee;
	}


	public EtatPlace getEtat() {
		return this.etat;
	}


	public void setEtat(EtatPlace etat) {
		this.etat = etat;
	}


	public Section getSection() {
		return this.section;
	}


	public Reservation getReservation() {
		return reservation;
	}


	public boolean libererPlace() {
		this.reservation = null;
		return etat.libererPlace();
	}

	public boolean confirmerPlace() {
		return etat.confirmerPlace();
	}

	public boolean reserverPlace(Reservation reservation) {
		this.reservation = reservation;
		return etat.reserverPlace();
	}
	
	public boolean estLibre() {
		if (etat == placeLibre)
			return true;
		return false;
	}

	public boolean estConfirmee() {
		if (etat == placeConfirmee)
			return true;
		return false;
	}

}