package database;

import database.compagnies.Compagnie;
import database.stations.Station;
import database.trajets.Trajet;
import database.transports.Transport;

import java.util.Calendar;
import java.util.List;

public class FabriqueTrajet {
    private static FabriqueTrajet instance=null;

    private FabriqueTrajet() {

    }

    public static FabriqueTrajet getInstance() {
        if (instance == null) {
            instance = new FabriqueTrajet();
        }
        return instance;
    }

    public Trajet fabriquer(String aType, Transport transport, Compagnie
            compagnie, List<Station> stationsTrajet,
                                String id, Calendar dateDepart, Calendar
                                        dateArrivee) {
        Trajet voyage = null;

        switch (aType.toLowerCase()) {

            case "vol":
                voyage = new database.trajets.Vol(transport, compagnie,
                        stationsTrajet, id, dateDepart, dateArrivee);
                break;
            case "trajettrain":
                voyage = new database.trajets.TrajetTrain(transport,
                        compagnie, stationsTrajet, id, dateDepart,
                        dateArrivee);
                break;
            case "croisiere":
                voyage = new database.trajets.Croisiere(transport, compagnie,
                        stationsTrajet, id, dateDepart,
                        dateArrivee);
                break;
        }
        return voyage;
    }
}