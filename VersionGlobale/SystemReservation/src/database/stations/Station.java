package database.stations;

import database.*;
import java.util.*;
import database.trajets.*;
import patrons.Visiteur;

public abstract class Station implements IVisitable {

	private List<Trajet> trajets;
	private String id;
	private String ville;
	private String nom;

	public Station(String id, String ville, String nom) {
		
		this.id = id;
		this.ville = ville;
		this.nom = nom;
		this.trajets = new ArrayList<>();
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVille() {
		return this.ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public List<Trajet> getTrajets(){
		return this.trajets;
	}

	public String getAttribut(String nomAttribut) {
		switch (nomAttribut.toLowerCase()) {
		case "id":
			return id;
		case "ville":
			return ville;
		case "nom":
			return nom;
		default:
			return null;
		}
	}

	public boolean setAttribut(String nomAttribut, String value) {
		switch(nomAttribut.toLowerCase()) {
		case "ville":
			ville = value;
			break;
		case "nom":
			nom = value;
			break;
		default:
			return false;
		}
		return true;
	}
	/**
	 * 
	 * @param v
	 */
	public void accept(Visiteur v) {
		v.visit(this);
	}

}
