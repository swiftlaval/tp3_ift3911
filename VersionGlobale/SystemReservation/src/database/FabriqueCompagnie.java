package database;

import database.compagnies.*;

public class FabriqueCompagnie {

	private static FabriqueCompagnie instance=null;


	private FabriqueCompagnie() {

	}

	public synchronized static FabriqueCompagnie getInstance() {
		if(instance==null){
			instance=new FabriqueCompagnie();
		}
		return instance;
	}

	public Compagnie fabriquer(String aType,String id, double prix, String
			nom) {
		Compagnie voyage = null;
		switch(aType.toLowerCase()){
			case "compagnieaerienne": voyage= new CompagnieAerienne(id, prix, nom);
			break;
			case "lignetrain": voyage= new LigneTrain(id, prix, nom); break;
			case "compagniecroisiere": voyage= new CompagnieCroisiere(id, prix, nom);
			break;
		}
		return voyage;
	}

}