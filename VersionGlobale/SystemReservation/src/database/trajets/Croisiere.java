package database.trajets;

import database.compagnies.Compagnie;
import database.stations.Station;
import database.transports.Transport;

import java.util.Calendar;
import java.util.List;

public class Croisiere extends Trajet {


	public Croisiere(Transport transport, Compagnie compagnie, List<Station> stationsTrajet, String id, Calendar dateDepart, Calendar dateArrivee) {
		super(transport, compagnie, stationsTrajet, id, dateDepart, dateArrivee);
	}
}