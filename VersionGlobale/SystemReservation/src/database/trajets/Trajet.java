package database.trajets;

import database.*;
import database.transports.*;
import patrons.Visiteur;
import database.compagnies.*;
import java.util.*;
import database.sections.*;
import database.stations.*;
import database.places.*;

public abstract class Trajet implements IVisitable {

	private Transport transport;
	private Compagnie compagnie;
	private List<Section> sectionsParTrajet;
	private List<Station> stationsTrajet;
	private String id;
	private Calendar dateDepart;
	private Calendar dateArrivee;

	public Trajet(Transport transport, Compagnie compagnie, List<Station> stationsTrajet, String id, Calendar dateDepart, Calendar dateArrivee) {
		this.transport = transport;
		this.compagnie = compagnie;
		this.stationsTrajet = stationsTrajet;
		this.id = id;
		this.dateDepart = dateDepart;
		this.dateArrivee = dateArrivee;
		this.sectionsParTrajet = new ArrayList<>();
		transport.getTrajets().add(this);
		compagnie.getTrajetsOfferts().add(this);
		stationsTrajet.stream().forEach(st -> st.getTrajets().add(this));
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Calendar getDateDepart() {
		return this.dateDepart;
	}

	public void setDateDepart(Calendar dateDepart) {
		this.dateDepart = dateDepart;
	}

	public Calendar getDateArrivee() {
		return this.dateArrivee;
	}

	public void setDateArrivee(Calendar dateArrivee) {
		this.dateArrivee = dateArrivee;
	}

	public Transport getTransport() {
		return transport;
	}

	public void setTransport(Transport transport) {
		this.transport = transport;
	}

	public Compagnie getCompagnie() {
		return compagnie;
	}

	public void setCompagnie(Compagnie compagnie) {
		this.compagnie = compagnie;
	}

	public List<Section> getSectionsParTrajet() {
		return sectionsParTrajet;
	}

	public void setSectionsParTrajet(List<Section> sectionsParTrajet) {
		this.sectionsParTrajet = sectionsParTrajet;
	}

	public List<Station> getStationsTrajet() {
		return stationsTrajet;
	}

	public void setStationsTrajet(List<Station> stationsTrajet) {
		this.stationsTrajet = stationsTrajet;
	}

	public Calendar getAttribut(String nomAttribut) {
		switch(nomAttribut.toLowerCase()) {
		case "dateDepart":
			return dateDepart;
		case "dateArrivee":
			return dateArrivee;
		default:
			return null;
		}
	}

	public boolean setAttribut(String nomAttribut, Calendar valeur) {
		switch(nomAttribut.toLowerCase()) {
		case "datedepart":
			if (valeur.after(dateArrivee))
				return false;
			dateDepart = valeur;
			break;
		case "datearrivee":
			if (dateDepart.after(valeur))
				return false;
			dateArrivee = valeur;
			break;
		default:
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param v
	 */
	public void accept(Visiteur v) {
		v.visit(this);
		compagnie.accept(v);;
		
		ListIterator<Station> iStation = stationsTrajet.listIterator();
		while(iStation.hasNext()){
			iStation.next().accept(v);
		}
		
		ListIterator<Section> iSection = sectionsParTrajet.listIterator();
		while(iSection.hasNext()){
			iSection.next().accept(v);;
		}
	}

	public Station getDestination() {
		return stationsTrajet.get(0);
	}

	public Station getOrigine() {
		return stationsTrajet.get(stationsTrajet.size()-1);
	}

	/**
	 * 
	 * @param priorite
	 * @param section
	 */
	public Place trouverPlace(int priorite, String section) {
		// TODO - implement Trajet.trouverPlace
		throw new UnsupportedOperationException();
	}

}