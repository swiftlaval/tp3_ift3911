package database.trajets;

import database.compagnies.Compagnie;
import database.stations.Station;
import database.transports.Transport;

import java.util.Calendar;
import java.util.List;

public class TrajetTrain extends Trajet {


    public TrajetTrain(Transport transport, Compagnie compagnie, List<Station> stationsTrajet, String id, Calendar dateDepart, Calendar dateArrivee) {
        super(transport, compagnie, stationsTrajet, id, dateDepart, dateArrivee);
    }
}