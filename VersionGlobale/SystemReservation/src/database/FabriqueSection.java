package database;

import database.dispositions.*;
import database.places.Place;
import database.places.SiegeAvion;
import database.sections.Affaire;
import database.sections.EcoPremium;
import database.sections.Economie;
import database.sections.Famille;
import database.sections.FamilleDeluxe;
import database.sections.Interieure;
import database.sections.Premiere;
import database.sections.PremiereT;
import database.sections.Section;
import database.sections.Suite;
import database.sections.VueSurOcean;

import java.util.List;

public class FabriqueSection {

    private static FabriqueSection instance;

    private FabriqueSection() {
    	// constructeur prive
    }

    public synchronized static FabriqueSection getInstance() {
        if (instance == null) {
            instance = new FabriqueSection();
        }
        return instance;
    }

    /**
     * Fabriquer une section
     * Types de section:
     * avion: premiere (F), affaire (A), economiquePremium (P), economique (E)
     * classes croisiere: interieure (I), vurSurOcean (O), suite (S), famille (F) et familleDeluxe (D)
     * classes train: premiereT (P), economie (F)
     *
     * @param typeDisposition
     * Disposition sieges:
     * Etroit (S) : trois colonnes avec une aile entre les colonnes 1 et 2
     * Confort (C) : quatre colonnes avec une aile entre les colonnes 2 et 3
     * Moyen (M) : six colonnes avec une aile entre 3 et 4
     * Large (L) : dix colonnes avec une aile entre les colonnes 3 et 4, et entre 7 et 8
     * 
     * @param nbRangees
     * @return
     */
    public Section fabriquer(String typeSection, String typeDisposition, int nbRangees, double prix) {
        Disposition disposition = null;
        switch (typeDisposition.toLowerCase()) {
        case "etroit":
        	disposition = new Etroit();
        	break;
        case "confort":
        	disposition = new Confort();
        	break;
        case "moyen":
        	disposition = new Moyen();
        	break;
        case "large":
        	disposition = new Large();
        	break;
        }
        switch(typeSection.toLowerCase()) {
        case "premiere":
        	return new Premiere(disposition, nbRangees, prix);
        case "affaire":
        	return new Affaire(disposition, nbRangees, prix);
        case "economiquepremium":
        	return new EcoPremium(disposition, nbRangees, prix);
        case "interieure":
        	return new Interieure(new DispositionCabine(), nbRangees, prix);
        case "vuesurocean":
        	return new VueSurOcean(new DispositionCabine(), nbRangees, prix);
        case "suite":
        	return new Suite(new DispositionCabine(), nbRangees, prix);
        case "famille":
        	return new Famille(new DispositionCabine(), nbRangees, prix);
        case "familledeluxe":
        	return new FamilleDeluxe(new DispositionCabine(), nbRangees, prix);
        case "premieret":
        	return new PremiereT(new EtroitT(), nbRangees, prix);
        case "economie":
        	return new Economie(new EtroitT(), nbRangees, prix);
        default:
        	return null;
        }
    }
}
