package database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import database.compagnies.*;
import database.places.Place;
import database.sections.Affaire;
import database.sections.EcoPremium;
import database.sections.Economie;
import database.sections.Economique;
import database.sections.Famille;
import database.sections.FamilleDeluxe;
import database.sections.Interieure;
import database.sections.Premiere;
import database.sections.PremiereT;
import database.sections.Section;
import database.sections.Suite;
import database.sections.VueSurOcean;
import database.trajets.*;
import database.transports.Transport;
import patrons.Observateur;
import patrons.Sujet;
import database.stations.*;

public class Gestionnaire implements Sujet {

	private FabriqueCompagnie fabriqueCompagnie;
	private List<Compagnie> listCompagnies;
	private List<Trajet> listTrajets;
	private FabriqueTrajet fabriqueTrajet;
	private List<Station> listStations;
	private FabriqueStation fabriqueStation;
	private List<Observateur> observateurs;
	private String message;
	private boolean changed;
	private List<Station> copieStations;
	private List<Compagnie> copieCompagnies;
	private List<Trajet> copieTrajets;

	private static Gestionnaire instance;

	public static Gestionnaire getInstance() {
		if (instance == null)
			instance = new Gestionnaire();
		return instance;
	}

	private Gestionnaire() {
		this.observateurs = new ArrayList<>();
		this.listCompagnies = new ArrayList<>();
		this.listTrajets = new ArrayList<>();
		this.listStations = new ArrayList<>();
		this.fabriqueCompagnie = FabriqueCompagnie.getInstance();
		this.fabriqueTrajet = FabriqueTrajet.getInstance();
		this.fabriqueStation = FabriqueStation.getInstance();
		this.copieStations = new ArrayList<>();
		this.copieCompagnies = new ArrayList<>();
		this.copieCompagnies = new ArrayList<>();
		instance = this;
	}
	
	public void postMessage(String msg){
		this.message = msg;
		this.changed = true;
		notifierObservateurs();
	}
	
	@Override
	public List<Observateur> getObservateurs() {
		return observateurs;
	}

	@Override
	public void abonner(Observateur o) {
		if(o == null) throw new NullPointerException("Observateur null!");
		if(!observateurs.contains(o)){
			observateurs.add(o);
			o.setSujet(this);
			System.out.println("L'observateur "+o.getClass().getName()+" est abonn� au " + this.getClass().getName()+"\n");
		}
	}

	@Override
	public void desabonner(Observateur o) {
		observateurs.remove(o);
		System.out.println("L'observateur "+o.getClass().getName()+" est desabonn� au " + this.getClass().getName()+"\n");
		
	}

	@Override
	public void notifierObservateurs() {
		if(!changed)
			return;
		this.changed = false;
		for (Observateur o : this.observateurs)
			o.update();
	}

	@Override
	public Object getUpdate(Observateur o) {
		return this.message;
	}

	/**
	 * 
	 * @param idCompagnie
	 * @param typeCompagnie
	 * @param prix
	 * @param nom
	 * @return
	 */
	public boolean creerCompagnie(String idCompagnie, String typeCompagnie, Double prix, String nom) {
		if (listCompagnies.stream().anyMatch(compagnie -> compagnie.getId().equals(idCompagnie)))
			return false;
		Compagnie compagnie = (Compagnie)fabriqueCompagnie.fabriquer(typeCompagnie, idCompagnie, prix, nom);
		listCompagnies.add(compagnie);
		postMessage("La compagnie "+typeCompagnie+" "+nom +" est cr�e");
		return true;
	}

	/**
	 * 
	 * @param idStation
	 * @param ville
	 * @param typeStation
	 * @param nomStation
	 * @return
	 */
	public boolean creerStation(String idStation, String ville, String typeStation, String nomStation) {
		if (listStations.stream().anyMatch(station -> station.getId().equals(idStation)))
			return false;
		Station station = (Station)fabriqueStation.fabriquer(typeStation, idStation, ville, nomStation);
		listStations.add(station);
		postMessage("La station "+typeStation+" " +nomStation +" est cr�e");
		return true;
	}

	/**
	 * 
	 * @param idCompagnie
	 * @param idTransport
	 * @param typeTransport
	 * @return
	 */
	public boolean creerTransport(String idCompagnie, String idTransport, String typeTransport) {
		Compagnie compagnie = listCompagnies.stream().filter(cie -> cie.getId().equals(idCompagnie)).findAny().orElse(null);
		if (compagnie == null)
			return false;
		if (compagnie.getTransports().stream().anyMatch(transport -> transport.getId().equals(idTransport)))
			return false;
		FabriqueTransport.getInstance().fabriquer(typeTransport, idTransport, compagnie);
		postMessage("Le transport "+typeTransport+" "+idTransport +" est cr�e");
		return true;
	}

	/**
	 * 
	 * @param idCompagnie
	 * @param idTransport
	 * @param typeSection
	 * @param disposition
	 * @param nbRangees
	 * @return
	 */
	public boolean ajouterSection(String idCompagnie, String idTransport, String typeSection, String disposition,String nbRangees) {
		Compagnie compagnie = listCompagnies.stream().filter(cie -> cie.getId().equals(idCompagnie)).findAny().orElse(null);
		if (compagnie == null)
			return false;

		Transport transport = compagnie.getTransports().stream().filter(tr -> tr.getId().equals(idTransport)).findAny().orElse(null);
		if (transport == null)
			return false;
		FabriqueSection.getInstance().fabriquer(typeSection, disposition, Integer.parseInt(nbRangees), compagnie.getPrix());
		postMessage("Une section "+typeSection+" est ajout� au transport "+transport.getId());
		throw new UnsupportedOperationException();
	}

	/**
	 *
	 * @param idCompagnie
	 * @param idTransport
	 * @param idTrajet
	 * @param typeTrajet
	 * @param dateDepart
	 * @param dateArrivee
	 * @param idStations
	 * @return
	 */
	public boolean creerTrajet(String idCompagnie, String idTransport, String idTrajet, String typeTrajet, Calendar dateDepart, Calendar dateArrivee, String...idStations) {
		if (listTrajets.stream().anyMatch(trajet -> trajet.getId().equals(idTrajet)))
			return false;
		Compagnie compagnie = listCompagnies.stream().filter(cie -> cie.getId().equals(idCompagnie)).findAny().orElse(null);
		if (compagnie == null)
			return false;
		Transport transport = compagnie.getTransports().stream().filter(tr -> tr.getId().equals(idTransport)).findAny().orElse(null);
		if (transport == null)
			return false;
		List<Station> stationsTrajet = new ArrayList<>();
		for (String idStation : idStations) {
			Station station = listStations.stream().filter(s -> s.getId().equals(idStation)).findAny().orElse(null);
			if (station == null)
				return false;
			stationsTrajet.add(station);
		}
		List<Section> sectionsParTrajet = new ArrayList<>();
		for (Section section : transport.getSections()) {
			//TODO: pour chaque section du transport, creer une copie de la section et ajouter � sectionsParTrajet 
		}
		Trajet trajet = (Trajet)fabriqueTrajet.fabriquer(typeTrajet, transport, compagnie, stationsTrajet, idTrajet, dateDepart, dateArrivee);
		listTrajets.add(trajet);
		postMessage("Le trajet "+typeTrajet+" "+trajet.getId()+ " est cr�e");
		return true;
	}

	/**
	 * 
	 * @param id
	 * @param attribut
	 * @param nouvelleValeur
	 */
	public boolean modifierCompagnie(String id, String attribut, String nouvelleValeur) {
		Optional<Compagnie> compagnie = listCompagnies.stream().filter(cie -> cie.getId().equals(id)).findAny();
		if (!compagnie.isPresent())
			return false;
		postMessage("Le " + attribut + " de la compagnie "+compagnie.get().getAttribut("nom") + " est modifi� � "+nouvelleValeur);
		return compagnie.get().setAttribute(attribut, nouvelleValeur);
	}

	/**
	 * 
	 * @param id
	 * @param attribut
	 * @param nouvelleValeur
	 */
	public boolean modifierStation(String id, String attribut, String nouvelleValeur) {
		Optional<Station> station = listStations.stream().filter(st -> st.getId().equals(id)).findAny();
		if (!station.isPresent())
			return false;
		postMessage("Le " + attribut + " de la station "+station.get().getAttribut("nom") + " est modifi� � "+nouvelleValeur);
		return station.get().setAttribut(attribut, nouvelleValeur);
	}

	/**
	 * 
	 * @param id
	 * @param attribut
	 * @param nouvelleValeur
	 * @return
	 */
	public boolean modifierTrajet(String id, String attribut, Calendar nouvelleValeur) {
		Optional<Trajet> trajet = listTrajets.stream().filter(tr -> tr.getId().equals(id)).findAny();
		if (!trajet.isPresent())
			return false;
		postMessage("Le " + attribut + " du trajet "+trajet.get().getAttribut("id") + " est modifi� � "+nouvelleValeur);
		return trajet.get().setAttribut(attribut, nouvelleValeur);
	}

	/**
	 * 
	 * @param id
	 */
	public boolean supprimerCompagnie(String id) {
		Optional<Compagnie> compagnie = listCompagnies.stream().filter(cie -> cie.getId().equals(id)).findAny();
		if (!compagnie.isPresent())
			return false;
		postMessage("La compagnie "+compagnie.get().getAttribut("nom") + " est supprim�e");
		compagnie.get().getTrajetsOfferts().forEach(trajet -> {
			listTrajets.remove(trajet);
			trajet.getOrigine().getTrajets().remove(trajet);
			trajet.getDestination().getTrajets().remove(trajet);
		});
		listCompagnies.remove(compagnie.get());
		return true;
	}

	/**
	 * 
	 * @param id
	 */
	public boolean supprimerStation(String id) {
		Optional<Station> station = listStations.stream().filter(s -> s.getId().equals(id)).findAny();
		if (!station.isPresent())
			return false;
		postMessage("La station "+station.get().getAttribut("nom") + " est supprim�e");
		station.get().getTrajets().forEach(trajet -> {
			trajet.getCompagnie().getTrajetsOfferts().remove(trajet);
			trajet.getTransport().getTrajets().remove(trajet);
			listTrajets.remove(trajet);
		});
		listStations.remove(station.get());
		return true;
	}

	/**
	 * 
	 * @param idCompagnie
	 * @param idTransport
	 * @return
	 */
	public boolean supprimerTransport(String idCompagnie, String idTransport) {
		Optional<Compagnie> compagnie = listCompagnies.stream().filter(cie -> cie.getId().equals(idCompagnie)).findAny();
		if (!compagnie.isPresent())
			return false;
		Transport transport = compagnie.get().getTransports().stream().filter(tr -> tr.getId().equals(idTransport)).findAny().orElse(null);
		if (transport == null)
			return false;
		postMessage("Le transport "+transport.getId()+ " est supprim�e");
		transport.getTrajets().forEach(trajet -> {
			trajet.getCompagnie().getTrajetsOfferts().remove(trajet);
			trajet.getOrigine().getTrajets().remove(trajet);
			trajet.getDestination().getTrajets().remove(trajet);
			listTrajets.remove(trajet);
		});
		compagnie.get().getTransports().remove(transport);
		return true;
	}

	/**
	 * 
	 * @param id
	 */
	public boolean supprimerTrajet(String id) {
		Trajet trajet = listTrajets.stream().filter(tr -> tr.getId().equals(id)).findAny().orElse(null);
		if (trajet == null)
			return false;
		postMessage("Le trajet "+trajet.getId()+ " est supprim�e");
		trajet.getOrigine().getTrajets().remove(trajet);
		trajet.getDestination().getTrajets().remove(trajet);
		trajet.getTransport().getTrajets().remove(trajet);
		trajet.getCompagnie().getTrajetsOfferts().remove(trajet);
		listTrajets.remove(trajet);
		return true;
	}
	
	/**
	 * 
	 * @param idStation
	 * @return
	 */
	public Station trouverStation(String idStation) {
		return listStations.stream().filter(station -> station.getId().equals(idStation)).findAny().orElse(null);
	}
	
	/**
	 * 
	 * @param idCompagnie
	 * @return
	 */
	public Compagnie trouverCompagnie(String idCompagnie) {
		return listCompagnies.stream().filter(cie -> cie.getId().equals(idCompagnie)).findAny().orElse(null);
	}
	
	public Transport trouverTransport(String idCompagnie, String idTransport) {
		Compagnie compagnie = listCompagnies.stream().filter(cie -> cie.getId().equals(idCompagnie)).findAny().orElse(null);
		if (compagnie == null)
			return null;
		return compagnie.getTransports().stream().filter(transport -> transport.getId().equals(idTransport)).findAny().orElse(null);
	}

	public Trajet trouverTrajet(String idTrajet) {
		return listTrajets.stream().filter(trajet -> trajet.getId().equals(idTrajet)).findAny().orElse(null);
	}

	/**
	 * 
	 * @param stationID
	 */
	public List<Trajet> rechercheParStation(String stationID) {
		List<Trajet> trajets = null;
		ListIterator<Station> iS = listStations.listIterator();
		while(iS.hasNext()){
			Station st = iS.next();
			if(st.getId().equals(stationID)){
				trajets = st.getTrajets();
				break;
			}
		}
		return trajets;	}

	/**
	 * 
	 * @param compagnieID
	 */
	public List<Trajet> rechercheParCompagnie(String compagnieID) {
		List<Trajet> trajets = null;
		ListIterator<Compagnie> iC = listCompagnies.listIterator();
		while(iC.hasNext()){
			Compagnie comp = iC.next();
			if(comp.getId().equals(compagnieID)){
				trajets = comp.getTrajetsOfferts();
				break;
			}
		}
		return trajets;
	}

	/**
	 * 
	 * @param origine
	 * @param destination
	 * @param date
	 * @param section
	 */
	public List<Trajet> recherche(String origine, String destination, String date, String section) {
		List<Trajet> res = new ArrayList<Trajet>();
		ListIterator<Trajet> i = listTrajets.listIterator();
		while(i.hasNext()){
			Trajet t = i.next();
			SimpleDateFormat sdf= new SimpleDateFormat("dd-MM-yyyy HH:mm");
			try {
				Calendar clendarDate= Calendar.getInstance();
				clendarDate.setTime(sdf.parse(date));
				if((t.getOrigine().getId().equals(origine)) &&
					(t.getDestination().getId().equals(destination)) && (t.getDateDepart().equals(clendarDate) )){

					List<Section> sections = t.getSectionsParTrajet();
					ListIterator<Section> iSection = sections.listIterator();
					while(iSection.hasNext()){
						Section s = iSection.next();
						if((s.getClass().getName().equals(section)) && (s.getNbPlacesLibres() != 0)){
							res.add(t);
						}
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
				System.err.println("FORMAT DE DATE INVALIDE, bon format: dd-MM-yyyy HH:mm");
			}
		}
		return res;
	}

	public void ajouterStation(Station station) {
		listStations.add(station);
		for (Trajet trajet : station.getTrajets()) {
			trajet.getCompagnie().getTrajetsOfferts().add(trajet);
			trajet.getTransport().getTrajets().add(trajet);
			listTrajets.add(trajet);
		}
	}

	public void ajouterCompagnie(Compagnie cie) {
		listCompagnies.add(cie);
		for (Trajet trajet : cie.getTrajetsOfferts()) {
			trajet.getOrigine().getTrajets().add(trajet);
			trajet.getDestination().getTrajets().add(trajet);
			trajet.getTransport().getTrajets().add(trajet);
			listTrajets.add(trajet);
		}
	}

	public void ajouterTransport(Transport transport) {
		Compagnie cie = transport.getCompagnie();
		cie.getTransports().add(transport);
		for (Trajet trajet : transport.getTrajets()) {
			cie.getTrajetsOfferts().add(trajet);
			trajet.getOrigine().getTrajets().add(trajet);
			trajet.getDestination().getTrajets().add(trajet);
			listTrajets.add(trajet);
		}
	}

	public void ajouterTrajet(Trajet trajet) {
		listTrajets.add(trajet);
		trajet.getCompagnie().getTrajetsOfferts().add(trajet);
		trajet.getTransport().getTrajets().add(trajet);
		trajet.getDestination().getTrajets().add(trajet);
		trajet.getOrigine().getTrajets().add(trajet);
	}
}