package database;

import database.stations.*;

public class FabriqueStation {

    private static FabriqueStation instance=null;


    private FabriqueStation() {

    }

    public synchronized static FabriqueStation getInstance() {
        if (instance == null) {
            instance = new FabriqueStation();
        }
        return instance;
    }

    public IVisitable fabriquer(String aType, String id, String ville, String
			nom) {
        IVisitable voyage = null;
        switch (aType.toLowerCase()) {
            case "aeroport":
                voyage = new Aeroport(id, ville, nom);
                break;
            case "gare":
                voyage = new Gare(id, ville, nom);
                break;
            case "port":
                voyage = new Port(id, ville, nom);
                break;
        }
        return voyage;
    }

}