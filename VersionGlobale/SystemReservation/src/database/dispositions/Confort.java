package database.dispositions;

import java.util.ArrayList;
import java.util.List;

import database.places.Cabine;
import database.places.Place;
import database.places.SiegeAvion;
import database.places.SiegeTrain;

public class Confort implements Disposition {

	private int nbColonnes = 4;
	private String positionAile = "2-3";
	// 0 1 2 3
	// 2 1 1 2

	public Confort() {
		// constructeur
	}

	@Override
	public List<Place> construirePlaces(String classe, int nbRangees) {
		List<Place> places = new ArrayList<>();
		for (int i = 0; i < nbRangees; i++) {
			for (int col = 0; col < nbColonnes; col ++) {
				Place place = new SiegeAvion();
				if (i%col == 0) {
					place.setEmplacement(2);
					place.setId(classe + (i + 1) + "A");
				}
				else if (i%col == 1) {
					place.setEmplacement(1);
					place.setId(classe + (i + 1) + "B");
				}
				else if (i%col == 2) {
					place.setEmplacement(1);
					place.setId(classe + (i + 1) + "C");
				}
				else {
					place.setEmplacement(2);
					place.setId(classe + (i + 1) + "D");
				}
				places.add(place);
			}
		}
		return places;
	}
}