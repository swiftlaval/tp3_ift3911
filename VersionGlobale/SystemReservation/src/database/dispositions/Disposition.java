package database.dispositions;

import java.util.List;

import database.places.Place;

public interface Disposition {

	public List<Place> construirePlaces(String classe, int nbRangees);
}