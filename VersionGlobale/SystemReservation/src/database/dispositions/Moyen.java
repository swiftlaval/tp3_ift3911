package database.dispositions;

import java.util.ArrayList;
import java.util.List;

import database.places.Cabine;
import database.places.Place;
import database.places.SiegeAvion;
import database.places.SiegeTrain;

public class Moyen implements Disposition {

	private int nbColonnes = 6;
	private String positionAile = "3-4";
	// 0 1 2 3 4 5
	// 2 0 1 1 0 2

	public Moyen() {
		// constructeur
	}

	@Override
	public List<Place> construirePlaces(String classe, int nbRangees) {
		List<Place> places = new ArrayList<>();
		for (int i = 0; i < nbRangees; i++) {
			for (int col = 0; col < nbColonnes; col ++) {
				Place place = new SiegeAvion();
				if (i%col == 0) {
					place.setEmplacement(2);
					place.setId(classe + (i + 1) + "A");
				}
				else if (i%col == 1) {
					place.setEmplacement(0);
					place.setId(classe + (i + 1) + "B");
				}
				else if (i%col == 2){
					place.setEmplacement(1);
					place.setId(classe + (i + 1) + "C");
				}
				else if (i%col == 3){
					place.setEmplacement(1);
					place.setId(classe + (i + 1) + "D");
				}
				else if (i%col == 4){
					place.setEmplacement(0);
					place.setId(classe + (i + 1) + "E");
				}
				else{
					place.setEmplacement(2);
					place.setId(classe + (i + 1) + "F");
				}
				places.add(place);
			}
		}
		return places;
	}

}