package database.dispositions;

import java.util.ArrayList;
import java.util.List;

import database.places.Cabine;
import database.places.Place;

public class DispositionCabine implements Disposition {

	public DispositionCabine() {
		// constructeur
	}

	@Override
	public List<Place> construirePlaces(String classe, int nbCabines) {
		List<Place> places = new ArrayList<>();
		for (int i = 0; i < nbCabines; i++) {
			Place place = new Cabine();
			place.setEmplacement(0);
			place.setId(classe);
			places.add(place);
		}
		return places;
	}

}
