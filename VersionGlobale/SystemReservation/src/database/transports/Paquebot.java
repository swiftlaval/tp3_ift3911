package database.transports;

import database.compagnies.Compagnie;

public class Paquebot extends Transport {

    public Paquebot(String id, Compagnie idCompagnie) {
        super(id,idCompagnie);
    }
}