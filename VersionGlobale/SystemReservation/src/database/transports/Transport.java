package database.transports;

import database.compagnies.*;
import java.util.*;
import database.sections.*;
import database.trajets.*;

public abstract class Transport {

	private String id;
	private Compagnie compagnie;
	private List<Section> sections;
	private List<Trajet> trajets;

	public Transport(String id, Compagnie compagnie) {
		this.id = id;
		this.compagnie = compagnie;
		this.sections = new ArrayList<>();
		this.trajets = new ArrayList<>();
		compagnie.getTransports().add(this);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Compagnie getCompagnie() {
		return compagnie;
	}
	
	public void setCompagnie(Compagnie compagnie) {
		this.compagnie = compagnie;
	}
	
	public List<Section> getSections() {
		return sections;
	}
	
	public void setSections(List<Section> sections) {
		this.sections = sections;
	}
	
	public List<Trajet> getTrajets() {
		return trajets;
	}
	
	public void setTrajets(List<Trajet> trajets) {
		this.trajets = trajets;
	}
}