package database.transports;

import database.compagnies.Compagnie;

public class Train extends Transport {

    public Train(String id, Compagnie idCompagnie) {
        super(id,idCompagnie);
    }
}