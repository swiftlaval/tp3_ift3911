package database.compagnies;

import database.*;
import java.util.*;
import database.transports.*;
import patrons.Visiteur;
import database.trajets.*;

public abstract class Compagnie implements IVisitable {

	private List<Transport> transports;
	private List<Trajet> trajetsOfferts;
	private String id;
	private double prix;
	private String nom;

	public Compagnie(String id, double prix, String nom) {
		this.id = id;
		this.prix = prix;
		this.nom = nom;
		this.transports = new ArrayList<>();
		this.trajetsOfferts = new ArrayList<>();
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Transport> getTransports() {
		return this.transports;
	}

	public List<Trajet> getTrajetsOfferts() {
		return this.trajetsOfferts;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getAttribut(String nomAttribut) {
		switch (nomAttribut.toLowerCase()) {
		case "id":
			return this.id;
		case "prix":
			return String.valueOf(this.prix);
		case "nom":
			return this.nom;
		default: return null;
		}
	}

	/**
	 * 
	 * @param v
	 */
	public void accept(Visiteur v) {
		v.visit(this);
	}

	public boolean setAttribute(String nomAttribut, String value) {
		switch(nomAttribut.toLowerCase()) {
		case "prix":
			prix = Double.parseDouble(value);
			break;
		case "nom":
			nom = value;
			break;
		default:
			return false;
		}
		return true;
	}
}
