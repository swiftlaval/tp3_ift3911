package database;

import patrons.Visiteur;

public interface IVisitable {

	/**
	 * 
	 * @param v
	 */
	void accept(Visiteur v);

}