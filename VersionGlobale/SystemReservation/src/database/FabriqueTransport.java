package database;

import database.compagnies.Compagnie;
import database.transports.Transport;

public class FabriqueTransport {
    private static FabriqueTransport instance=null;


    private FabriqueTransport() {

    }

    public synchronized static FabriqueTransport getInstance() {
        if (instance == null) {
            instance = new FabriqueTransport();
        }
        return instance;
    }

    public Transport fabriquer(String aType, String id, Compagnie idCompagnie) {
        Transport voyage = null;
        switch (aType.toLowerCase()) {
            case "avion":
                voyage = new database.transports.Avion(id,idCompagnie);
                break;
            case "paquebot":
                voyage = new database.transports.Paquebot(id,idCompagnie);
                break;
            case "train":
                voyage = new database.transports.Train(id,idCompagnie);
                break;
        }
        return voyage;
    }
}
