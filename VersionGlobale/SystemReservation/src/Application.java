import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import administration.Administrateur;
import administration.SystemAdmin;
import client.Client;
import client.SystemClient;
import database.Gestionnaire;
import database.compagnies.Compagnie;
import database.compagnies.CompagnieAerienne;
import database.compagnies.CompagnieCroisiere;
import database.compagnies.LigneTrain;
import database.stations.Station;
import database.transports.Transport;
import patrons.Observateur;
import reservations.ControleurReservation;

//Class main
public class Application {
	static String input="";
	static Scanner scan;
	static SystemAdmin sysAdmin;
	static Gestionnaire gestionnaire;
	static ControleurReservation controlReserve;
	static SystemClient sysClient;
	
	public static void main(String[]args) throws ParseException {
		//In order to have some records in the DB, for consultation cases!
		sysAdmin = SystemAdmin.getInstance();
		sysClient = SystemClient.getInstance();
		gestionnaire = Gestionnaire.getInstance();
		controlReserve = ControleurReservation.getInstance();
		createInstances();
		scan = new Scanner(System.in);
		chooseUser();
	}
	
	public static void chooseUser() {
		printMessage("Veilliez entrez 'a' pour acceder au syst�me admin ou 'c' pour le syst�me client");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("a")) {
			VoletAdministratif admin = new VoletAdministratif(input,scan,sysAdmin);
			admin.adminManageTripBy();
			
		}else if(input.equalsIgnoreCase("c")) {
			VoletClient client = new VoletClient(input,scan);
			client.clientManageTripBy();
		}
		
	}

	private static void createInstances() throws ParseException {
		//Create Admins
		Administrateur matiar = new Administrateur("Matiar","1111");
		Administrateur lygia = new Administrateur("Lygia","2222");
		Administrateur moncef = new Administrateur("Moncef","3333");
		Administrateur rosalie = new Administrateur("Rosalie","4444");
		
		sysAdmin.getAdmins().add(matiar);
		sysAdmin.getAdmins().add(lygia);
		sysAdmin.getAdmins().add(moncef);
		sysAdmin.getAdmins().add(rosalie);		
		gestionnaire.abonner(sysAdmin);
		controlReserve.abonner(sysClient);
		
		//Create Vol
		//Montreal;Toronto;2018-05-01;Affaire   dd-MM-yyyy HH:mm
		sysAdmin.creerStation("aer1", "Montreal", "aeroport", "MontrealAirport");
		sysAdmin.creerStation("aer2", "Toronto", "aeroport", "TorontoAirport");
		sysAdmin.creerStation("por1", "Moscow", "port", "MoscowPort");
		sysAdmin.creerStation("por2", "Paris", "port", "ParisPort");
		sysAdmin.creerStation("gar1", "Montreal", "gare", "Gare1");
		sysAdmin.creerStation("gar2", "London", "gare", "Gare2");

		sysAdmin.creerCompagnie("ca1", "compagnieAerienne", 123.4, "compAir1");
		sysAdmin.creerCompagnie("ca2", "compagnieCroisiere", 113.4, "compCroisi1");
		sysAdmin.creerCompagnie("ca3", "compagnieCroisiere", 113.4, "compCroisi1");

		sysAdmin.creerTransport("ca1", "trans1", "avion");
		sysAdmin.creerTransport("ca2", "trans2", "paquebot");
		//sysAdmin.ajouterSection("ca1", "trans1", "Affaire", "Confort", "10");

		sysAdmin.creerTrajet("ca1", "trans1", "vol1", "vol", "2018-05-01 00:00", "2018-05-02 03:03", "aer1","aer2");
		sysAdmin.creerTrajet("ca2", "trans2", "cro2", "croisiere", "2018-05-01 00:00", "2018-05-02 03:03", "por1","por2");

		String res1 = sysClient.reserverPlace("vol1", 1);
		String res2 = sysClient.reserverPlace("vol1",2);
		sysClient.paiementReservation(res1,"lapin","123 rue ","lapin@gmail.com","514555555","02-02-1900","78797987","02-2030","798797987","03-2021");
		sysClient.paiementReservation(res2,"lapin2","1700 rue ","lapin2@gmail.com","123123123","02-02-1900","78297987","02-2030","798797987","03-2021");


		String res3 = sysClient.reserverPlace("cro1", 1);
		String res4 = sysClient.reserverPlace("cro2",2);
		sysClient.paiementReservation(res3,"lapin3","45645 rue ","lapindfdf@gmail.com","514575555","02-02-1900","78797987","02-2030","798797987","03-2021");
		sysClient.paiementReservation(res4,"lapin4","7897 rue ","lapindfdfd@gmail.com","12317123","02-02-1900","78295687","02-2030","798797987","03-2021");


	}
	
	public static Calendar getDate(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date dateTime = sdf.parse(date);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, dateTime.getYear());
		cal.set(Calendar.MONTH, dateTime.getMonth());
		cal.set(Calendar.DAY_OF_MONTH, dateTime.getDay());
		cal.set(Calendar.HOUR_OF_DAY, dateTime.getHours());
		cal.set(Calendar.MINUTE, dateTime.getMinutes());		
		return cal;
	}

	static void printMessage(String msg) {
		System.out.println(msg);
	}
	
	
}
