package patrons;
import database.trajets.*;
import database.compagnies.*;
import database.stations.*;
import database.sections.*;

public interface Visiteur {

	/**
	 * 
	 * @param trajet
	 */
	void visit(Trajet trajet);

	/**
	 * 
	 * @param comp
	 */
	void visit(Compagnie comp);

	/**
	 * 
	 * @param station
	 */
	void visit(Station station);

	/**
	 * 
	 * @param section
	 */
	void visit(Section section);

}