package patrons;
import java.util.*;

public interface Sujet {
	public List<Observateur> getObservateurs();
	public void abonner(Observateur o);
	public void desabonner(Observateur o);
	public void notifierObservateurs();
	public Object getUpdate(Observateur o);

}