package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import administration.SystemAdmin;
import database.FabriqueCompagnie;
import database.FabriqueTrajet;
import database.Gestionnaire;
import database.compagnies.Compagnie;
import database.trajets.Trajet;

public class CommandeTest {

	private static SystemAdmin systemAdmin = SystemAdmin.getInstance();
	private static Gestionnaire gestionnaire = Gestionnaire.getInstance();
	
	public static void main(String[] args) {

		creerCompagnie();
		creerStation();
		creerTransport();
		creerTrajet();
		modifierCompagnie();
		modifierStation();
		modifierTrajet();
	  
	}
	
	public static void creerCompagnie() {
		boolean cree = systemAdmin.creerCompagnie("TP3", "compagnieaerienne", 100.00, "Compagnie TP");
		System.out.println("Compagnie TP3 cree: " + cree);
	}

	public static void creerStation() {
		boolean cree = systemAdmin.creerStation("MO1", "Montreal", "aeroport", "Aeroport TP3 de Montreal");
		System.out.println("Station MO1 cree: " + cree);
		cree = systemAdmin.creerStation("TO1", "Toronto", "aeroport", "Aeroport TP3 de Toront");
		System.out.println("Station TO1 cree: " + cree);
	}
	
	public static void creerTransport() {
		boolean cree = systemAdmin.creerTransport("TP3", "AE1", "avion");
		System.out.println("Transport AE1 cree: " + cree);
	}
	
	public static void creerTrajet() {
		boolean cree = systemAdmin.creerTrajet("TP3", "AE1", "TP3-1", "vol", "18-04-2018 11:00", "18-04-2018 13:00", "MO1", "TO1");
		System.out.println("Trajet TP3-1 cree: " + cree);
	}
	
	public static void modifierCompagnie() {
		boolean modifie = systemAdmin.modifierCompagnie("TP3", "nom", "Nouvelle Compagnie TP");
		System.out.println("Nom compagnie TP3 modifie: " + modifie);
		modifie = systemAdmin.undo();
		System.out.println("Commande defaite: " + modifie);
		modifie = systemAdmin.modifierCompagnie("TP3", "prix", "120.00");
		System.out.println("Prix compagnie TP3 modifie: " + modifie);
	}
	
	public static void modifierStation() {
		boolean modifie = systemAdmin.modifierStation("MO1", "nom", "Aeroport TP3 de Montreal et Region");
		System.out.println("Nom station MO1 modifie: " + modifie);
		modifie = systemAdmin.modifierStation("MO1", "ville", "Dorval");
		System.out.println("Ville station MO1 modifie: " + modifie);
	}
	
	public static void modifierTrajet() {
		boolean modifie = systemAdmin.modifierTrajet("TP3-1", "dateDepart", "18-04-2017 11:30");
		System.out.println("Depart trajet TP3-1 modifie: " + modifie);
	}
}
