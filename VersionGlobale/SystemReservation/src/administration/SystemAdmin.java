package administration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import administration.PatronCommande.*;
import database.*;
import database.compagnies.Compagnie;
import database.trajets.Trajet;
import database.transports.Transport;
import patrons.Observateur;
import patrons.Sujet;

public class SystemAdmin implements Observateur {

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	private static SystemAdmin instance;
	private ControleAdmin controle;
	private Gestionnaire gestionnaire;
	private VisiteurAdmin visiteur;
	private List<Administrateur> admins;
	private Sujet sujet;

	/**
	 * True si la derniere consultation concernait les Station
	 */
	private boolean consulterStation;
	private String consultationID;

	private SystemAdmin() {
		this.setAdmins(new ArrayList<>());
		this.controle = new ControleAdmin();
		this.gestionnaire = Gestionnaire.getInstance();
		this.visiteur = new VisiteurAdmin();
		this.consulterStation = false;
		this.consultationID = "";
		instance = this;
	}
	
	public static SystemAdmin getInstance() {
		if (instance == null)
			instance = new SystemAdmin();
		return instance;
	}
	
	@Override
	public void update() {
		String msg = (String) sujet.getUpdate(this);
		if(msg == null){
			System.out.println(":: Pas de nouvelles!");
		}else{
			getAdmins().forEach(admin -> System.out.println(admin.getNom()+"! Un messge vaut votre attention!\n:: Message::"+msg));
		}
		
	}

	@Override
	public void setSujet(Sujet s) {
		this.sujet = s;
		
	}
	
	public Sujet getSujet() {
		return sujet;
	}

	/**
	 * 
	 * @param idCompagnie
	 * @param typeCompagnie
	 * @param prix
	 * @param nomCompagnie
	 * @return
	 */
	public boolean creerCompagnie(String idCompagnie, String typeCompagnie, Double prix, String nomCompagnie) {
		controle.setCommande(new CommandeCreerCompagnie(gestionnaire));
		return controle.executerCommande(idCompagnie, typeCompagnie, prix.toString(), nomCompagnie);
	}

	/**
	 * 
	 * @param idStation
	 * @param ville
	 * @param typeStation
	 * @param nomStation
	 * @return
	 */
	public boolean creerStation(String idStation, String ville, String typeStation, String nomStation) {
		controle.setCommande(new CommandeCreerStation(gestionnaire));
		return controle.executerCommande(idStation, ville, typeStation, nomStation);
	}

	/**
	 * 
	 * @param idCompagnie
	 * @param idTransport
	 * @param typeTransport
	 * @return
	 */
	public boolean creerTransport(String idCompagnie, String idTransport, String typeTransport) {
		controle.setCommande(new CommandeCreerTransport(gestionnaire));
		return controle.executerCommande(idCompagnie, idTransport, typeTransport);
	}

	/**
	 * 
	 * @param idCompagnie
	 * @param idTransport
	 * @param typeSection
	 * @param disposition
	 * @return
	 */
	public boolean ajouterSection(String idCompagnie, String idTransport, String typeSection, String disposition, String nbPlaces) {
		controle.setCommande(new CommandeAjouterSection(gestionnaire));
		return controle.executerCommande(idCompagnie, idTransport, typeSection, disposition,nbPlaces);
	}

	/**
	 * 
	 * @param idCompagnie
	 * @param idTransport
	 * @param idTrajet
	 * @param typeTrajet
	 * @param dateDepart
	 * @param dateArrivee
	 * @param idStations
	 * @return
	 */
	public boolean creerTrajet(String idCompagnie, String idTransport, String idTrajet, String typeTrajet, String dateDepart, String dateArrivee, String...idStations) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date depart = new Date();
		Date arrivee = new Date();
		try {
			depart = sdf.parse(dateDepart);
			arrivee = sdf.parse(dateArrivee);
		} catch (ParseException e) {
			return false;
		}
		if (depart.getTime() >= arrivee.getTime())
			return false;
		Calendar calDepart = Calendar.getInstance();
		calDepart.setTime(depart);
		Calendar calArrivee = Calendar.getInstance();
		calArrivee.setTime(arrivee);
		controle.setCommande(new CommandeCreerTrajet(gestionnaire));
		Object[] parametres = new Object[6 + idStations.length];
		parametres[0] = idCompagnie;
		parametres[1] = idTransport;
		parametres[2] = idTrajet;
		parametres[3] = typeTrajet;
		parametres[4] = calDepart;
		parametres[5] = calArrivee;
		for (int i=0; i<idStations.length; i++) {
			parametres[6+i] = idStations[i];
		}
		return controle.executerCommande(parametres);
	}

	/**
	 * 
	 * @param id
	 * @param attribut
	 * @param nouvelleValeur
	 */
	public boolean modifierCompagnie(String id, String attribut, String nouvelleValeur) {
		controle.setCommande(new CommandeModifierCompagnie(gestionnaire));
		return controle.executerCommande(id, attribut, nouvelleValeur);
	}

	/**
	 * 
	 * @param id
	 * @param attribut
	 * @param nouvelleValeur
	 */
	public boolean modifierStation(String id, String attribut, String nouvelleValeur) {
		controle.setCommande(new CommandeModifierStation(gestionnaire));
		return controle.executerCommande(id, attribut, nouvelleValeur);
	}

	/**
	 * 
	 * @param id
	 * @param attribut dateDepart ou dateArrivee
	 * @param nouvelleValeur String format "dd-MM-yyyy HH:mm"
	 */
	public boolean modifierTrajet(String id, String attribut, String nouvelleValeur) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date date = new Date();
		try {
			date = sdf.parse(nouvelleValeur);
		} catch (ParseException e) {
			return false;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		controle.setCommande(new CommandeModifierTrajet(gestionnaire));
		return controle.executerCommande(id, attribut, calendar);
	}

	/**
	 * 
	 * @param id
	 */
	public boolean supprimerCompagnie(String id) {
		controle.setCommande(new CommandeSupprimerCompagnie(gestionnaire));
		return controle.executerCommande(id);
	}

	/**
	 * 
	 * @param id
	 */
	public boolean supprimerStation(String id) {
		controle.setCommande(new CommandeSupprimerStation(gestionnaire));
		return controle.executerCommande(id);
	}

	/**
	 * 
	 * @param idCompagnie
	 * @param id
	 * @return
	 */
	public boolean supprimerTransport(String idCompagnie, String id) {
		controle.setCommande(new CommandeSupprimerTransport(gestionnaire));
		return controle.executerCommande(idCompagnie, id);
	}

	/**
	 * 
	 * @param id
	 */
	public boolean supprimerTrajet(String id) {
		controle.setCommande(new CommandeSupprimerTrajet(gestionnaire));
		return controle.executerCommande(id);
	}
	
	/**
	 * Undo de la derniere commande executee (pas de support a redo)
	 */
	public boolean undo() {
		return controle.unexecuterCommande();
	}

	/**
	 * 
	 * @param idStation
	 */
	public List<String> consultationParStation(String idStation) {
		consulterStation = true;
		consultationID = idStation;
		List<Trajet> trajets = gestionnaire.rechercheParStation(idStation);
		List<String> res = new ArrayList<String>();
		if(trajets != null){
		ListIterator<Trajet> i = trajets.listIterator();
			while(i.hasNext()){
				i.next().accept(visiteur);
				res.add(visiteur.getResultat());
			}
		}
		return res;
	}

	/**
	 * 
	 * @param idCompagnie
	 */
	public List<String> consultationParCompagnie(String idCompagnie) {
		consulterStation = false;
		consultationID = idCompagnie;
		List<Trajet> trajets = gestionnaire.rechercheParCompagnie(idCompagnie);
		List<String> res = new ArrayList<String>();
		if(trajets != null){
			ListIterator<Trajet> i = trajets.listIterator();
			while(i.hasNext()){
				i.next().accept(visiteur);
				res.add(visiteur.getResultat());
			}
		}
		return res;
	}

	public List<Administrateur> getAdmins() {
		return admins;
	}

	public void setAdmins(List<Administrateur> admins) {
		this.admins = admins;
	}

}