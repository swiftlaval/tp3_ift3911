package administration;

import database.trajets.*;
import patrons.Visiteur;

import java.util.Calendar;

import database.compagnies.*;
import database.stations.*;
import database.sections.*;

public class VisiteurAdmin implements Visiteur {

	private String stations;
	private String compagnie;
	private String idTrajet;
	private String dates;
	private String infoSections;
	private String resultat;
	
	public VisiteurAdmin() {
		
	}

	public String getResultat() {
		if(stations.endsWith("-")){
			stations = stations.substring(0, stations.length() - 1);
		}
		resultat = stations + compagnie + idTrajet + dates + infoSections; 
		return this.resultat;
	}

	/**
	 * 
	 * @param trajet
	 */
	public void visit(Trajet trajet) {
		idTrajet = trajet.getId();
		Calendar calD = trajet.getDateDepart();
		Calendar calA = trajet.getDateArrivee();
		String date1 = "(" + calD.get(Calendar.YEAR)+ "." + (calD.get(Calendar.MONTH) +1) + "." +
				calD.get(Calendar.DAY_OF_MONTH) + ":" + calD.get(Calendar.HOUR_OF_DAY) + ":" +
				calD.get(Calendar.MINUTE);
		String date2 = "-" + calA.get(Calendar.YEAR)+ "." + (calA.get(Calendar.MONTH) +1) + "." +
				calA.get(Calendar.DAY_OF_MONTH) + ":" + calA.get(Calendar.HOUR_OF_DAY) + ":" +
				calA.get(Calendar.MINUTE) + ")";
		dates = date1 + date2;
		stations = "";
		infoSections = "";
	}

	/**
	 * 
	 * @param comp
	 */
	public void visit(Compagnie comp) {
		compagnie = ":[" + comp.getId() + "]";
	}

	/**
	 * 
	 * @param station
	 */
	public void visit(Station station) {
		stations += station.getId() + "-";
	}

	/**
	 * 
	 * @param section
	 */
	public void visit(Section section) {
		infoSections += "|" + section.getClass().getName().charAt(0) + section.getDisposition().getClass().getName().charAt(0)
				+ "(" + section.getNbPlacesLibres() + "/" + section.getNbPlacesTotal() + ")" + section.getPrix();
	}


}