package administration.PatronCommande;

public interface Commande {

	/**
	 * 
	 * @param parametres
	 */
	boolean execute(Object... parametres);

	boolean undo();

}