package administration.PatronCommande;

import database.*;
import database.trajets.Trajet;

public class CommandeSupprimerTrajet implements Commande {

	private Gestionnaire gestionnaire;
	private Trajet trajetSupprime;

	/**
	 * 
	 * @param gestionnaire
	 */
	public CommandeSupprimerTrajet(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
	}

	/**
	 * 
	 * @param parametres
	 */
	@Override()
	public boolean execute(Object... parametres) {
		trajetSupprime = gestionnaire.trouverTrajet((String)parametres[0]);
		if (trajetSupprime == null)
			return false;
		return gestionnaire.supprimerTrajet((String)parametres[0]);
	}

	@Override()
	public boolean undo() {
		if (trajetSupprime == null)
			return false;
		gestionnaire.ajouterTrajet(trajetSupprime);
		trajetSupprime = null;
		return true;
	}

}