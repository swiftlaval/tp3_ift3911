package administration.PatronCommande;

import database.*;

public class CommandeCreerStation implements Commande {

	private String idStation;
	private Gestionnaire gestionnaire;

	/**
	 * 
	 * @param gestionnaire
	 */
	public CommandeCreerStation(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
		this.idStation = "";
	}

	/**
	 * 
	 * @param parametres dans l'ordre: idStation, ville, typeStation, nomStation
	 */
	@Override()
	public boolean execute(Object... parametres) {
		boolean cree = gestionnaire.creerStation((String)parametres[0], (String)parametres[1], (String)parametres[2], (String)parametres[3]);
		if (cree) {
			idStation = (String)parametres[0];
		}
		return cree;
	}

	@Override()
	public boolean undo() {
		return gestionnaire.supprimerStation(idStation);
	}

}