package administration.PatronCommande;

import database.*;

public class CommandeCreerTransport implements Commande {

	private String idCompagnie;
	private String idTransport;
	private Gestionnaire gestionnaire;

	/**
	 * 
	 * @param gestionnaire
	 */
	public CommandeCreerTransport(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
		this.idTransport = "";
	}

	/**
	 * 
	 * @param parametres
	 */
	@Override()
	public boolean execute(Object... parametres) {
		boolean cree = gestionnaire.creerTransport((String)parametres[0], (String)parametres[1], (String)parametres[2]);
		if (cree) {
			idCompagnie = (String)parametres[0];
			idTransport = (String)parametres[1];
		}
		return cree;
	}

	@Override()
	public boolean undo() {
		return gestionnaire.supprimerTransport(idCompagnie, idTransport);
	}

}