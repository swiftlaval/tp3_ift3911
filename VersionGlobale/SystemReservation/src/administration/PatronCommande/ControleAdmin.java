package administration.PatronCommande;

public class ControleAdmin {

	private Commande commande;

	public void setCommande(Commande commande) {
		this.commande = commande;
	}

	public boolean executerCommande(Object...parametres) {
		return commande.execute(parametres);
	}

	public boolean unexecuterCommande() {
		return commande.undo();
	}

}