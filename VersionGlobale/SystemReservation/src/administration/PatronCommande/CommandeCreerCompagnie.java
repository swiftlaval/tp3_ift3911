package administration.PatronCommande;

import database.*;

public class CommandeCreerCompagnie implements Commande {

	private String idCompagnie;
	private Gestionnaire gestionnaire;

	/**
	 * 
	 * @param gestionnaire
	 */
	public CommandeCreerCompagnie(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
		this.idCompagnie = "";
	}

	/**
	 * 
	 * @param parametres dans l'ordre: idCompagnie, typeCompagnie, Double prix, nom
	 * @return 
	 */
	@Override()
	public boolean execute(Object... parametres) {
		boolean cree = gestionnaire.creerCompagnie((String)parametres[0], (String)parametres[1], Double.parseDouble((String)parametres[2]), (String)parametres[3]);
		if (cree) {
			idCompagnie = (String)parametres[0];
		}
		return cree;
	}

	@Override()
	public boolean undo() {
		return gestionnaire.supprimerCompagnie(idCompagnie);
	}

}