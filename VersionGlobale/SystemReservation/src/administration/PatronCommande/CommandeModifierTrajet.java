package administration.PatronCommande;

import java.util.Calendar;

import database.*;
import database.trajets.Trajet;

public class CommandeModifierTrajet implements Commande {

	private Gestionnaire gestionnaire;
	private String idTrajet;
	private String attributModifie;
	private Calendar ancienneValeur;

	/**
	 * 
	 * @param gestionnaire
	 */
	public CommandeModifierTrajet(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
	}

	/**
	 * 
	 * @param parametres dans l'ordre: idTrajet, nomAttribut, nouvelleValeur
	 */
	@Override()
	public boolean execute(Object... parametres) {
		Trajet trajet = gestionnaire.trouverTrajet((String)parametres[0]);
		if (trajet != null)
			ancienneValeur = trajet.getAttribut((String)parametres[1]);
		boolean modifie = gestionnaire.modifierTrajet((String)parametres[0], (String)parametres[1], (Calendar)parametres[2]);
		if (modifie) {
			idTrajet = (String)parametres[0];
			attributModifie = (String)parametres[1];
		}
		return modifie;
	}

	@Override()
	public boolean undo() {
		if (idTrajet == null)
			return false;
		return gestionnaire.modifierTrajet(idTrajet, attributModifie, ancienneValeur);
	}

}