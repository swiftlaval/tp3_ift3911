package administration.PatronCommande;

import database.*;
import database.compagnies.Compagnie;

public class CommandeModifierCompagnie implements Commande {

	private Gestionnaire gestionnaire;
	private String idCompagnie;
	private String attributModifie;
	private String ancienneValeur;

	/**
	 * 
	 * @param gestionnaire
	 */
	public CommandeModifierCompagnie(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
	}

	/**
	 * 
	 * @param parametres dans l'ordre: idCompagnie, nomAttribut, nouvelleValeur
	 */
	@Override()
	public boolean execute(Object... parametres) {
		Compagnie compagnie = gestionnaire.trouverCompagnie((String)parametres[0]);
		if (compagnie != null) {
			ancienneValeur = compagnie.getAttribut((String)parametres[1]);
		}
		boolean modifie = gestionnaire.modifierCompagnie((String)parametres[0], (String)parametres[1], (String)parametres[2]);
		if (modifie) {
			idCompagnie = (String)parametres[0];
			attributModifie = (String)parametres[1];
		}
		return modifie;
	}

	@Override()
	public boolean undo() {
		if (idCompagnie == null)
			return false;
		return gestionnaire.modifierCompagnie(idCompagnie, attributModifie, ancienneValeur);
	}

}