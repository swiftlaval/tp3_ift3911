package administration.PatronCommande;

import database.*;
import database.transports.Transport;

public class CommandeSupprimerTransport implements Commande {

	private Gestionnaire gestionnaire;
	private Transport transportSupprime;

	/**
	 * 
	 * @param gestionnaire
	 */
	public CommandeSupprimerTransport(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
	}

	/**
	 * 
	 * @param parametres
	 */
	@Override()
	public boolean execute(Object... parametres) {
		transportSupprime = gestionnaire.trouverTransport((String)parametres[0], (String)parametres[1]);
		if (transportSupprime == null)
			return false;
		return gestionnaire.supprimerTransport((String)parametres[0], (String)parametres[1]);
	}

	@Override()
	public boolean undo() {
		if (transportSupprime == null)
			return false;
		gestionnaire.ajouterTransport(transportSupprime);
		transportSupprime = null;
		return true;
	}

}