package administration.PatronCommande;

import database.*;
import database.stations.Station;

public class CommandeSupprimerStation implements Commande {

	private Gestionnaire gestionnaire;
	private Station stationSupprimee;

	/**
	 * 
	 * @param gestionnaire
	 */
	public CommandeSupprimerStation(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
	}

	/**
	 *
	 * @param parametres
	 */
	@Override()
	public boolean execute(Object... parametres) {
		stationSupprimee = gestionnaire.trouverStation((String)parametres[0]);
		if (stationSupprimee == null)
			return false;
		return gestionnaire.supprimerStation((String)parametres[0]);
	}

	@Override()
	public boolean undo() {
		if (stationSupprimee == null)
			return false;
		gestionnaire.ajouterStation(stationSupprimee);
		stationSupprimee = null;
		return true;
	}

}