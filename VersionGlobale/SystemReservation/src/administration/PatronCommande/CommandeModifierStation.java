package administration.PatronCommande;

import database.*;
import database.compagnies.Compagnie;
import database.stations.Station;

public class CommandeModifierStation implements Commande {

	private Gestionnaire gestionnaire;
	private String idStation;
	private String attributModifie;
	private String ancienneValeur;

	/**
	 * 
	 * @param gestionnaire
	 */
	public CommandeModifierStation(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
	}

	/**
	 * 
	 * @param parametres dans l'ordre: idStation, nomAttribut, nouvelleValeur
	 */
	@Override()
	public boolean execute(Object... parametres) {
		Station station = gestionnaire.trouverStation((String)parametres[0]);
		if (station != null) {
			ancienneValeur = station.getAttribut((String)parametres[1]);
		}
		boolean modifie = gestionnaire.modifierStation((String)parametres[0], (String)parametres[1], (String)parametres[2]);
		if (modifie) {
			idStation = (String)parametres[0];
			attributModifie = (String)parametres[1];
		}
		return modifie;
	}

	@Override()
	public boolean undo() {
		if (idStation == null)
			return false;
		return gestionnaire.modifierStation(idStation, attributModifie, ancienneValeur);
	}

}