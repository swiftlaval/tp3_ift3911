package administration.PatronCommande;

import database.Gestionnaire;

public class CommandeAjouterSection implements Commande {

	private Gestionnaire gestionnaire;

	public CommandeAjouterSection(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
	}

	@Override
	public boolean execute(Object... parametres) {
		return gestionnaire.ajouterSection((String)parametres[0], (String)parametres[1], (String)parametres[2], (String)parametres[3],(String)parametres[3]);
	}

	@Override
	public boolean undo() {
		// TODO Auto-generated method stub
		return false;
	}

}
