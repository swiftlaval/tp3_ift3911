package administration.PatronCommande;

import database.*;
import database.compagnies.Compagnie;

public class CommandeSupprimerCompagnie implements Commande {

	private Gestionnaire gestionnaire;
	private Compagnie compagnieSupprimee;

	/**
	 * 
	 * @param gestionnaire
	 */
	public CommandeSupprimerCompagnie(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
	}

	/**
	 * 
	 * @param parametres
	 */
	@Override()
	public boolean execute(Object... parametres) {
		compagnieSupprimee = gestionnaire.trouverCompagnie((String)parametres[0]);
		if (compagnieSupprimee == null)
			return false;
		return gestionnaire.supprimerCompagnie((String)parametres[0]);
	}

	@Override()
	public boolean undo() {
		if (compagnieSupprimee == null)
			return false;
		gestionnaire.ajouterCompagnie(compagnieSupprimee);
		compagnieSupprimee = null;
		return true;
	}

}