package administration.PatronCommande;

import java.util.Calendar;

import database.*;

public class CommandeCreerTrajet implements Commande {

	private String idTrajet;
	private Gestionnaire gestionnaire;

	/**
	 * 
	 * @param gestionnaire
	 */
	public CommandeCreerTrajet(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
		this.idTrajet = "";
	}

	/**
	 * 
	 * @param parametres dans l'ordre: idCompagnie, idTransport, idTrajet, typeTrajet, (Calendar)dateDepart, (Calendar)dateArrivee, String...idStations
	 */
	@Override()
	public boolean execute(Object... parametres) {
		String[] idStations = new String[parametres.length - 6];
		System.arraycopy(parametres, 6, idStations, 0, parametres.length - 6);
		boolean cree = gestionnaire.creerTrajet((String)parametres[0], (String)parametres[1], (String)parametres[2], (String)parametres[3], (Calendar)parametres[4], (Calendar)parametres[5], idStations);
		if (cree) {
			idTrajet = (String)parametres[0];
		}
		return cree;
	}

	@Override()
	public boolean undo() {
		return gestionnaire.supprimerTrajet(idTrajet);
	}

}