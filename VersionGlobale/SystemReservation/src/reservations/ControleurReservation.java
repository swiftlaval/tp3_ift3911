package reservations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import client.Client;
import database.*;
import database.places.Place;
import database.sections.Section;
import database.trajets.*;
import patrons.Observateur;
import patrons.Sujet;
import transactions.CarteCredit;
import transactions.Paiement;
import transactions.Transaction;

public class ControleurReservation implements Sujet {

	private List<Reservation> listReservations;
	private List<Client> clients;
	private Gestionnaire gestionnaire;
	private static ControleurReservation instance;
	private static Integer idActuel;
	private List<Observateur> observateurs;
	private String message;
	private boolean changed;

	private ControleurReservation() {
		this.observateurs = new ArrayList<>();
		this.listReservations = new ArrayList<>();
		this.clients = new ArrayList<>();
		this.gestionnaire = Gestionnaire.getInstance();
		idActuel = 0;
		instance = this;
	}

	public static ControleurReservation getInstance() {
		if (instance == null)
			return new ControleurReservation();
		return instance;
	}
	
	public void postMessage(String msg){
		this.message = msg;
		this.changed = true;
		notifierObservateurs();
	}

	@Override
	public List<Observateur> getObservateurs() {
		return observateurs;
	}

	@Override
	public void abonner(Observateur o) {
		if(o == null) throw new NullPointerException("Observateur null!");
		if(!observateurs.contains(o)){
			observateurs.add(o);
			o.setSujet(this);
			System.out.println("L'observateur "+o.getClass().getName()+" est abonn� au " + this.getClass().getName()+"\n");
		}
	}

	@Override
	public void desabonner(Observateur o) {
		observateurs.remove(o);
		System.out.println("L'observateur "+o.getClass().getName()+" est desabonn� au " + this.getClass().getName()+"\n");
		
	}

	@Override
	public void notifierObservateurs() {
		if(!changed)
			return;
		this.changed = false;
		for (Observateur o : this.observateurs)
			o.update();
	}

	@Override
	public Object getUpdate(Observateur o) {
		return this.message;
	}

	/**
	 * Reserver une place aleatoire dans un trajet d'apres la priorite donnee
	 * @param trajet
	 * @param priorite
	 * @return
	 */
	public String effectuerReservation(Trajet trajet, int priorite) {
		Section section = trajet.getSectionsParTrajet().stream().filter(sect -> sect.getNbPlacesLibres() > 0).findAny().orElse(null);
		if (section == null)
			return null;
		Place place = section.trouverPlace(priorite);
		if (place == null)
			return null;
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR, 24);
		listReservations.add(new Reservation((++idActuel).toString(), trajet, place, priorite, calendar.getTime()));
		return idActuel.toString();
	}

	/**
	 * Payer une reservation
	 * @param noReservation
	 * @param nomClient
	 * @param adresseClient
	 * @param courriel
	 * @param telephone
	 * @param dateNaissance
	 * @param numeroPasseport
	 * @param dateExpirationPasseport
	 * @param numeroCarteCredit
	 * @param dateExpirationCarte
	 * @return true si succes, false sinon
	 */
	public boolean payerReservation(String noReservation, String nomClient, String adresseClient, String courriel, 
			String telephone, String dateNaissance, String numeroPasseport, String dateExpirationPasseport, 
			String numeroCarteCredit, String dateExpirationCarte) {
		Reservation reservation = listReservations.stream().filter(res -> res.getNoReservation().equals(noReservation)).findAny().orElse(null);
		if (reservation == null)
			return false;
		Client client = clients.stream().filter(c -> c.getCourriel().equals(courriel)).findAny().orElse(null);
		if (client == null) {
			client = new Client(nomClient, adresseClient, courriel, telephone, dateNaissance, numeroPasseport, dateExpirationPasseport);	
		}
		else {
			client.update(nomClient, adresseClient, telephone, dateNaissance, numeroPasseport, dateExpirationPasseport);
		}
		return reservation.payerReservation(client, numeroCarteCredit, dateExpirationCarte);
	}

	/**
	 * Annuler une reservation
	 * @param noRes
	 * @param noCarteCredit numero carte de credit (null accepte si annulation de reservation non-confirmee) 
	 * @param dateExpiration de la carte (null accepte si annulation de reservation non-confirmee)
	 * @return true si succes, false sinon
	 */
	public boolean annulerReservation(String noRes, String noCarteCredit, String dateExpiration, double ratioRemboursement) {
		Reservation reservation = listReservations.stream().filter(res -> res.getNoReservation().equals(noRes)).findAny().orElse(null);
		if (reservation == null)
			return false;
		Trajet trajetReserve = reservation.getTrajet();
		long heuresAvantDepart = trajetReserve.getDateDepart().getTime().getTime()-new Date().getTime() / 1000*60*60; 
		if (heuresAvantDepart < 24)
			return false;
		if (trajetReserve.getClass().isInstance(Croisiere.class) && heuresAvantDepart < 72)
			return false;
		return reservation.annulerReservation(ratioRemboursement, noCarteCredit, dateExpiration);
	}

	/**
	 * Modifier le trajet d'une reservation
	 * Si le siege est confirme, le client ne peut changer que la date et l'heure du trajet sans frais.
	 * @param noReseservation
	 * @param nouveauTrajet
	 * @return
	 */
	public boolean modifierTrajetReservation(String noReservation, Trajet nouveauTrajet) {
		Reservation reservation = listReservations.stream().filter(res -> res.getNoReservation().equals(noReservation)).findAny().orElse(null);
		if (reservation == null)
			return false;
		Trajet trajetReserve = reservation.getTrajet();
		long heuresAvantDepart = trajetReserve.getDateDepart().getTime().getTime()-new Date().getTime() / 1000*60*60; 
		if (heuresAvantDepart < 24)
			return false;
		if (trajetReserve.getClass().isInstance(Croisiere.class) && heuresAvantDepart < 72)
			return false;
		if (reservation.estConfirmee()) {
			if (!trajetReserve.getOrigine().equals(nouveauTrajet.getOrigine()))
				return false;
			if (!trajetReserve.getDestination().equals(nouveauTrajet.getDestination()))
				return false;
		}
		Section sectionReservee = reservation.getPlaceReservee().getSection();
		int priorite = reservation.getPriorite();
		Section nouvelleSection = nouveauTrajet.getSectionsParTrajet().stream().filter(sect -> sect.getPrix() == sectionReservee.getPrix()).findAny().orElse(null);
		Place nouvellePlace = nouvelleSection.trouverPlace(priorite);
		if (nouvellePlace == null)
			return false;
		if (reservation.estConfirmee()) {
			return reservation.modifierTrajet(nouveauTrajet, nouvellePlace, null);
		}
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR, 24);
		return reservation.modifierTrajet(nouveauTrajet, nouvellePlace, calendar.getTime());
	}

	/**
	 * Modifier la classe du siege d'une reservation
	 * Si le siege est confirme, le client peut changer la classe du siege et payer la difference
	 * ou se faire rembourser la difference moins 10%
	 * @param noRes
	 * @param nouvelleSection
	 * @param noCarteCredit
	 * @param dateExpiration
	 * @param ratioTransaction
	 * @return
	 */
	public boolean modifierSectionReservation(String noRes, Section nouvelleSection, String noCarteCredit, String dateExpiration, double ratioTransaction) {
		Reservation reservation = listReservations.stream().filter(res -> res.getNoReservation().equals(noRes)).findAny().orElse(null);
		if (reservation == null)
			return false;
		Trajet trajetReserve = reservation.getTrajet();
		long heuresAvantDepart = trajetReserve.getDateDepart().getTime().getTime()-new Date().getTime() / 1000*60*60; 
		if (heuresAvantDepart < 24)
			return false;
		if (trajetReserve.getClass().isInstance(Croisiere.class) && heuresAvantDepart < 72)
			return false;
		Place nouvellePlace = nouvelleSection.trouverPlace(reservation.getPriorite());
		if (nouvellePlace == null)
			return false;
		if (reservation.estConfirmee())
			return reservation.modifierSection(nouvellePlace, null, noCarteCredit, dateExpiration, ratioTransaction);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR, 24);
		return reservation.modifierSection(nouvellePlace, calendar.getTime(), noCarteCredit, dateExpiration, ratioTransaction);
	}

}