package reservations;

import client.*;
import java.util.*;
import transactions.*;
import database.trajets.*;
import database.places.*;

public class Reservation {

	private String noReservation;
	private Trajet trajet;
	private Place placeReservee;
	/**
	 * 1 = aile, 2 = fenetre
	 */
	private int priorite;
	private Date dateExpiration;
	private Client client;
	private List<Transaction> transactions;

	public Reservation(String noReservation, Trajet trajet, Place placeReservee, int priorite, Date dateExpiration) {
		this.noReservation = noReservation;
		this.trajet = trajet;
		this.placeReservee = placeReservee;
		this.priorite = priorite;
		this.dateExpiration = dateExpiration;
		this.transactions = new ArrayList<>();
		placeReservee.reserverPlace(this);
	}

	public String getNoReservation() {
		return noReservation;
	}

	public Trajet getTrajet() {
		return trajet;
	}

	public Place getPlaceReservee() {
		return placeReservee;
	}

	public int getPriorite() {
		return priorite;
	}

	public Date getDateExpiration() {
		return dateExpiration;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public boolean payerReservation(Client client, String noCarteCredit, String dateExpiration) {
		boolean confirme = placeReservee.confirmerPlace();
		if (confirme) {
			this.client = client;
			CarteCredit carteCredit = new CarteCredit(noCarteCredit, dateExpiration);
			transactions.add(new Paiement(placeReservee.getSection().getPrix(), carteCredit));
			dateExpiration = null;
			return true;
		}
		return false;
	}

	public boolean annulerReservation(double ratioRemboursement, String noCarteCredit, String dateExpiration) {
		if (placeReservee.estConfirmee()) {
			CarteCredit carteCredit = new CarteCredit(noCarteCredit, dateExpiration);
			transactions.add(new Remboursement(placeReservee.getSection().getPrix()*ratioRemboursement, carteCredit));
		}
		return placeReservee.libererPlace();
	}

	public boolean estConfirmee() {
		return placeReservee.estConfirmee();
	}

	public boolean modifierTrajet(Trajet nouveauTrajet, Place nouvellePlace, Date nouvelleDateExpiration) {
		placeReservee.libererPlace();
		placeReservee = nouvellePlace;
		placeReservee.reserverPlace(this);
		trajet = nouveauTrajet;
		dateExpiration = nouvelleDateExpiration;
		return true;
	}
	
	public boolean modifierSection(Place nouvellePlace, Date nouvelleDateExpiration, String noCarteCredit, String dateExpiration, double ratioTransaction) {
		if (placeReservee.estConfirmee()) {
			CarteCredit carteCredit = new CarteCredit(noCarteCredit, dateExpiration);
			double difference = placeReservee.getSection().getPrix() - nouvellePlace.getSection().getPrix();
			if (difference > 0)
				transactions.add(new Remboursement(difference*ratioTransaction, carteCredit));
			else
				transactions.add(new Paiement(difference*ratioTransaction, carteCredit));
		}
		placeReservee.libererPlace();
		placeReservee = nouvellePlace;
		placeReservee.reserverPlace(this);
		return true;
	}
}