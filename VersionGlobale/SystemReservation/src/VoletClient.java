import java.util.Scanner;

import client.SystemClient;
import database.Gestionnaire;
import database.trajets.Croisiere;
import database.trajets.Trajet;
import database.trajets.TrajetTrain;
import database.trajets.Vol;

public class VoletClient {
    private static String input;
    private static Scanner scan;
    private SystemClient sysClient;
    private Gestionnaire gestionnaire;

    public VoletClient(String input, Scanner scan) {
        this.input = input;
        this.scan = scan;
        sysClient = SystemClient.getInstance();
        gestionnaire = Gestionnaire.getInstance();

    }

    private static void printMessage(String msg) {
        System.out.println(msg);
    }

    public void clientManageTripBy() {
        printMessage("SYSTEME CLIENT: \n Veuilliz choissir parmi ces options suivantes: (a)Avion - (p)Paquebot - (t)Train - (mp)Menu Precedant");
        input = scan.nextLine();
        if (input.equalsIgnoreCase("a")) {
            voyage();
        } else if (input.equalsIgnoreCase("p")) {
            voyage();
        } else if (input.equalsIgnoreCase("t")) {
            voyage();
        } else if (input.equalsIgnoreCase("mp")) {
            Application.chooseUser();
        }

    }

    private void voyage() {
        printMessage("	(r)Recherche - (a)Annulation de Réservation - (c)Changement de Réservation - (mp)Menu Precedant");
        input = scan.nextLine();
        if (input.equalsIgnoreCase("r")) {
            rechercherVol();
        } else if (input.equalsIgnoreCase("s")) {
            reserverVol();
        } else if (input.equalsIgnoreCase("a")) {
            annulerResVol();
        } else if (input.equalsIgnoreCase("c")) {
            modifierResVol();
        } else if (input.equalsIgnoreCase("mp")) {
            clientManageTripBy();
        }

    }

    private void rechercherVol() {
        printMessage("	Rechercher des vols. Origin;Destionation;Date Depart;Classe\n"
                + "	ex: 'Montreal;Toronto;2018-05-01;Affaire'");//TODO
        input = scan.nextLine();
        String inputs[] = input.split(";");
        try {
            sysClient.verificationTrajet(inputs[0], inputs[1], inputs[2], inputs[3]);
        } catch (NullPointerException e) {
            printMessage("Erreur: Input invalide!");
            rechercherVol();
        }
        catch (IndexOutOfBoundsException e) {
            printMessage("Erreur: Il vous manque un input!");
            rechercherVol();
        }
        printMessage("	\n(s)Reservation - (mp)Menu Precedant");
        input = scan.nextLine();
        if (input.equalsIgnoreCase("s")) {
            reserverVol();
        } else if (input.equalsIgnoreCase("mp")) {
            voyage();
        }

    }

    private void reserverVol() {
        printMessage("	Entrez le numero du vol et votre priorite.\n"
                + "	ex: 'hom999;fenetre'");//TODO
        input = scan.nextLine();
        String inputs[] = input.split(";");
        Vol v = (Vol) gestionnaire.trouverTrajet(inputs[0]);
        try {
            sysClient.reserverPlace(v.getId(), Integer.valueOf(inputs[1]));
        } catch (NullPointerException e) {
            printMessage("Erreur: Input invalide!");reserverVol();
        }
        catch (IndexOutOfBoundsException e) {
            printMessage("Erreur: Il vous manque un input!");reserverVol();
        }
        printMessage("	\n(p)Paiment - (mp)Menu Precedant");
        input = scan.nextLine();
        if (input.equalsIgnoreCase("mp")) {
            voyage();
        } else if (input.equalsIgnoreCase("p")) {
            paiement();
        }

    }

    private void annulerResVol() {
        printMessage("	Entrez votre numero de réservation. hint:111111 et votre numero de carte de credit et sa date d'expiration, separe par des ;\"");//TODO
        input = scan.nextLine();
        String inputs[] = input.split(";");
        try {
            sysClient.annulationReservation(inputs[0], inputs[1], inputs[2]);
        } catch (NullPointerException e) {
            printMessage("Erreur: Input invalide!");annulerResVol();
        }
        catch (IndexOutOfBoundsException e) {
            printMessage("Erreur: Il vous manque un input!");annulerResVol();
        }
        clientManageTripBy();

    }

    private void modifierResVol() {

        printMessage("	(r)Modifier la section - (s)Modifier la date de depart - (mp)Menu Precedant");
        input = scan.nextLine();
        if (input.equalsIgnoreCase("r")) {
            modifierSectionVol();
        } else if (input.equalsIgnoreCase("s")) {
            modifDateVol();
        } else if (input.equalsIgnoreCase("mp")) {
            clientManageTripBy();
        }


    }

    private void modifDateVol() {
        printMessage("	Entrez votre numero de réservation. hint:222222 et le nom de la nouvelle section(lettre) et votre numero de carte de credit et sa date d'expiration, separe par des ;");
        input = scan.nextLine();
        String inputs[] = input.split(";");

        try {
            sysClient.modifierTrajetReservation(inputs[0], inputs[1]);
        } catch (NullPointerException e) {
            printMessage("Erreur: Input invalide!");modifDateVol();
        }
          catch (IndexOutOfBoundsException e) {
            printMessage("Erreur: Il vous manque un input!");modifDateVol();
        }
        clientManageTripBy();
    }



    private void modifierSectionVol() {

        printMessage("	Entrez votre numero de réservation. hint:222222 et le nom de la nouvelle section(lettre) et votre numero de carte de credit et sa date d'expiration, separe par des ;");
        input = scan.nextLine();
        String inputs[] = input.split(";");
        Trajet c = (Croisiere) gestionnaire.trouverTrajet(inputs[0]);
        try {
            sysClient.modifierSectionReservation(inputs[0], c.getId(), inputs[2], inputs[3], inputs[4]);
        } catch (NullPointerException e) {
            printMessage("Erreur: Input invalide!");modifierSectionVol();
        }
        catch (IndexOutOfBoundsException e) {
            printMessage("Erreur: Il vous manque un input!");modifierSectionVol();
        }

        clientManageTripBy();
    }

    private void paiement() {
        printMessage("	Entrez votre numero de reservation;nom;adresse;telephone;date de naissance;numero de passport;date d'expiration passport; num carte de credit; date expiration carte de credit!");
        input = scan.nextLine();
        String inputs[] = input.split(";");
        try {
            sysClient.paiementReservation(inputs[0], inputs[1], inputs[2], inputs[3], inputs[4], inputs[5], inputs[6], inputs[7], inputs[8], inputs[9]);
        } catch (NullPointerException e) {
            printMessage("Erreur: Input invalide!");paiement();
        }
        catch (IndexOutOfBoundsException e) {
            printMessage("Erreur: Il vous manque un input!");paiement();
        }

        printMessage(" (mp)Rentrer au menu principle.");
        input = scan.nextLine();
        if (input.equalsIgnoreCase("mp")) {
            clientManageTripBy();
        }

    }


}












/*	private void croisier() {
		printMessage("	(r)Recherche - (s)Réservation - (a)Annulation de Réservation - (c)Changement de Réservation - (mp)Menu Precedant");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("r")) {
			rechercherCroisier();
		}else if(input.equalsIgnoreCase("s")) {
			reserverCroisier();
		}else if(input.equalsIgnoreCase("a")) {
			annulerResCroisier();
		}else if(input.equalsIgnoreCase("c")) {
			modifierResCroisier();
		}else if(input.equalsIgnoreCase("mp")) {
			clientManageTripBy();
		}

	}*/

/*	private void rechercherCroisier() {
		printMessage("	Rechercher des croisieres. Origin;Destionation;Date Depart;Classe\n"
				+ "	ex: 'Montreal;Manchester;2018-05-01;Suite'");//TODO
		input = scan.nextLine();
		String inputs [] = input.split(";");
		sysClient.verificationTrajet(inputs[0], inputs[1], inputs[2], inputs[3]);
		printMessage("	\n(s)Reservation - (mp)Menu Precedant");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("s")) {
			reserverCroisier();
		}else if(input.equalsIgnoreCase("mp")) {
			croisier();
		}

	}
	*/
/*	private void reserverCroisier() {
		printMessage("	Entrez le numero du vol et votre priorite.\n"
				+ "	ex: 'com7777;fenetre(2) aile(1)'");//TODO
		input = scan.nextLine();
		String inputs [] = input.split(";");
		Croisiere c = (Croisiere) gestionnaire.trouverTrajet(inputs[0]);
		sysClient.reserverPlace(c.getId(), Integer.valueOf(inputs[1]));
		printMessage("	\n(p)Paiment - (mp)Menu Precedant");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("mp")) {
			croisier();
		}else if(input.equalsIgnoreCase("p")) {
			paiement();
		}

	}*/

/*	private void annulerResCroisier() {
		printMessage("	Entrez votre numero de réservation. hint:333333 et votre numero de carte de credit et sa date d'expiration, separe par des ;");
		input = scan.nextLine();
		String inputs [] = input.split(";");
		sysClient.annulationReservation(inputs[0],inputs[1],inputs[2]);

	}*/

/*private void modifierResCroisier() {
 *//*
		printMessage("	Entrez votre numero de réservation. hint:444444 et votre numero de carte de credit et sa date d'expiration, separe par des ;");
		input = scan.nextLine();
		sysClient.modifierTrajetReservation(input);
		*//*
		printMessage("	(r)Modifier la section - (s)Modifier la date de depart - (mp)Menu Precedant");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("r")) {
			modifSectionCroisiere();
		}else if(input.equalsIgnoreCase("s")) {
			modifDateCroisiere();
		}else if(input.equalsIgnoreCase("mp")) {
			clientManageTripBy();
		}

	}

	private void modifSectionCroisiere() {
		printMessage("	Entrez votre numero de réservation. hint:444444 et le nom de la nouvelle section(lettre) et votre numero de carte de credit et sa date d'expiration, separe par des ;");
		input = scan.nextLine();
		String inputs [] = input.split(";");
		Trajet c = (Croisiere) gestionnaire.trouverTrajet(inputs[0]);
		sysClient.modifierSectionReservation(inputs[0],c.getId(),inputs[2],inputs[3],inputs[4]);
	}


	private void modifDateCroisiere() {
		//TODO
	}

	private void train() {
		printMessage("	(r)Recherche - (s)Réservation - (a)Annulation de Réservation - (c)Changement de Réservation - (mp)Menu Precedant");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("r")) {
			rechercherTrain();
		}else if(input.equalsIgnoreCase("s")) {
			reserverTrain();
		}else if(input.equalsIgnoreCase("a")) {
			annulerResTrain();
		}else if(input.equalsIgnoreCase("c")) {
			modifierResTrain();
		}else if(input.equalsIgnoreCase("mp")) {
			clientManageTripBy();
		}

	}

	private void rechercherTrain() {
		printMessage("	Rechercher des voyages trains. Origin;Destionation;Date Depart;Classe\n"
				+ "	ex: 'Montreal;Toronto;2018-05-01;Affaire'");//TODO
		input = scan.nextLine();
		String inputs [] = input.split(";");
		sysClient.verificationTrajet(inputs[0], inputs[1], inputs[2], inputs[3]);
		printMessage("	\n(s)Reservation - (mp)Menu Precedant");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("s")) {
			reserverTrain();
		}else if(input.equalsIgnoreCase("mp")) {
			train();
		}

	}

	private void reserverTrain() {
		printMessage("	Entrez le numero du croisiere et votre priorite.\n"
				+ "	ex: 'ton888;fenetre'");//TODO - ton888
		input = scan.nextLine();
		String inputs [] = input.split(";");
		TrajetTrain t = (TrajetTrain) gestionnaire.trouverTrajet(inputs[0]);
		sysClient.reserverPlace(t, inputs[1]);
		printMessage("	\n(p)Paiment - (mp)Menu Precedant");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("mp")) {
			train();
		}else if(input.equalsIgnoreCase("p")) {
			paiement();
		}

	}

	private void annulerResTrain() {
		printMessage("	Entrez votre numero de réservation. hint:555555");
		input = scan.nextLine();
		sysClient.annulationReservation(input);

	}

	private void modifierResTrain() {
		*//*printMessage("	Entrez votre numero de réservation. hint:666666");
		input = scan.nextLine();
		sysClient.modificationReservation(input);*//*
		printMessage("	(r)Modifier la section - (s)Modifier la date de depart - (mp)Menu Precedant");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("r")) {
			modifSectionTrain();
		}else if(input.equalsIgnoreCase("s")) {
			modifDateTrain();
		}else if(input.equalsIgnoreCase("mp")) {
			clientManageTripBy();
		}


	}*/