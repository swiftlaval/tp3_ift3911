import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

import administration.Administrateur;
import administration.SystemAdmin;
import database.Gestionnaire;
import database.compagnies.Compagnie;

public class VoletAdministratif{
	private static String input;
	private static Scanner scan;
	private static SystemAdmin sysAdmin;
	private static Gestionnaire gestionnaire;
	public VoletAdministratif(String input, Scanner scan, SystemAdmin sysAdmin) {
		this.input = input;
		this.scan = scan;
		this.sysAdmin = sysAdmin;
		
	}
			
	static void adminManageTripBy() {
		printMessage("SYSTEME ADMIN: \n username;password  hint: Matiar;1111 - Lygia;2222 - Moncef;3333 - Rosalie-4444");
		input = scan.nextLine();
		String[] userPass = input.split(";");
		Administrateur admin = sysAdmin.getAdmins().stream().filter(adm -> adm.getNom().equals(userPass[0])).findAny().orElse(null);
		if(input.equalsIgnoreCase("mp")) {
			Application.chooseUser();
		}else if (admin.getPassword().equals(userPass[1])) {
				printMessage("Bienvenu "+admin.getNom()+"! Veuilliez choissir parmi les options suivantes: (a)Avion - (p)Paquebot - (t)Train - (mp)Menu Precedant");
				input = scan.nextLine();
				if(input.equalsIgnoreCase("a")) {
					gestionVoyage("Vol","Aeroport" , "Aerienne" , "Avion");
				}else if(input.equalsIgnoreCase("p")) {
					gestionVoyage("Croisiers","Ports" , "Croisieres" , "Paquebot");
				}else if(input.equalsIgnoreCase("t")) {
					gestionVoyage("Trajet Train","Gares" , "Ligne Trains" , "Train");
				}else if(input.equalsIgnoreCase("mp")) {
					Application.chooseUser();
				}
			}else {
				printMessage("Quelque chose ne va pas! :(    R�essayez!");
				adminManageTripBy();
			}
		
	}
	
	private static void gestionVoyage(String typeVoyage, String typeStation , String typeCompagnie , String typeTransport) {
		printMessage("Systeme de gestion de "+typeVoyage+":\n (s)Gestion "+typeStation+" (c)Gestion Compagnie "+typeCompagnie+" - (t)Gestion "+typeVoyage+" - (g)Gestion Section de "+typeTransport+" - (x)Consultation - (mp)Menu Precedant");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("s")) {
			gestionStation(typeVoyage, typeStation , typeCompagnie , typeTransport);
		}else if(input.equalsIgnoreCase("c")) {
			gestionCompagnie(typeVoyage, typeStation , typeCompagnie , typeTransport);
		}else if(input.equalsIgnoreCase("t")) {
			gestionTrajet(typeVoyage, typeStation , typeCompagnie , typeTransport);
		}else if(input.equalsIgnoreCase("g")) {
			gestionSection(typeVoyage, typeStation , typeCompagnie , typeTransport);
		}else if(input.equalsIgnoreCase("x")) {
			consultation(typeVoyage, typeStation , typeCompagnie , typeTransport);
		}else if(input.equalsIgnoreCase("mp")) {
			adminManageTripBy();
		}
	}
	
	private static void gestionStation(String typeVoyage, String typeStation , String typeCompagnie , String typeTransport) {
		printMessage("	(add)Ajouter "+typeStation+" - (del) Supprimer "+typeStation+" - (mod) Modifier "+typeStation+" - (mp) Menu Precedent");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("add")) {
			printMessage(" Entrez 'id"+typeStation+";ville;nom"+typeStation);
			String inputs [] = scan.nextLine().split(";");
			if(inputs.length<3) {
				printMessage("Les entrees ne sont pas correct! Reessayez!");
				gestionStation(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
			sysAdmin.creerStation(inputs [0], inputs [1], typeStation, inputs [2]);
			printMessage(" (u)Undo - (c)Confirmer");
			input = scan.nextLine();
			if(input.equalsIgnoreCase("u")) {
				sysAdmin.undo();
				gestionStation(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}else if(input.equalsIgnoreCase("c")) {
				gestionStation(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
		}else if(input.equalsIgnoreCase("del")) {
			printMessage(" Entrez 'id"+typeStation+"'");
			input = scan.nextLine();
			sysAdmin.supprimerStation(input);
			printMessage(" (u)Undo - (c)Confirmer");
			input = scan.nextLine();
			if(input.equalsIgnoreCase("u")) {
				sysAdmin.undo();
				gestionStation(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}else if(input.equalsIgnoreCase("c")) {
				gestionStation(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
		}else if(input.equalsIgnoreCase("mod")) {
			printMessage(" Entrez 'id"+typeStation+";attribut;nouvelleValeur");
			String inputs [] = scan.nextLine().split(";");
			sysAdmin.modifierStation(inputs [0], inputs [1], inputs [2]);
			printMessage(" (u)Undo - (c)Confirmer");
			input = scan.nextLine();
			if(input.equalsIgnoreCase("u")) {
				sysAdmin.undo();
				gestionStation(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}else if(input.equalsIgnoreCase("c")) {
				gestionStation(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}

		}else if(input.equalsIgnoreCase("mp")) {
			gestionVoyage(typeVoyage, typeStation , typeCompagnie , typeTransport);
		}

	}


	
	private static void gestionCompagnie(String typeVoyage,String typeStation ,String typeCompagnie ,String typeTransport) {
		printMessage("	(add)Ajouter Compagnie "+typeCompagnie+" - (del) Supprimer Compagnie "+typeCompagnie+" - (mod) Modifier Compagnie "+typeCompagnie+" - (mp) Menu Precedent");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("add")) {
			printMessage(" Entrez 'idCompagnie"+typeCompagnie+";prix;nomCompagnie"+typeCompagnie);
			String inputs [] = scan.nextLine().split(";");
			if(inputs.length<3) {
				printMessage("Les entrees ne sont pas correct! Reessayez!");
				gestionCompagnie(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
			sysAdmin.creerCompagnie(inputs[0],"compagnie"+typeCompagnie, Double.parseDouble(inputs[1]), inputs[2]);
			printMessage(" (u)Undo - (c)Confirmer");
			input = scan.nextLine();
			if(input.equalsIgnoreCase("u")) {
				sysAdmin.undo();
				gestionCompagnie(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}else if(input.equalsIgnoreCase("c")) {
				gestionCompagnie(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
		}else if(input.equalsIgnoreCase("del")) {
			printMessage(" Entrez 'idCompagnie"+typeCompagnie);
			input = scan.nextLine();
			sysAdmin.supprimerCompagnie(input);
			printMessage(" (u)Undo - (c)Confirmer");
			input = scan.nextLine();
			if(input.equalsIgnoreCase("u")) {
				sysAdmin.undo();
				gestionCompagnie(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}else if(input.equalsIgnoreCase("c")) {
				gestionCompagnie(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
		}else if(input.equalsIgnoreCase("mod")) {
			printMessage(" Entrez 'idCompagnie"+typeCompagnie+";attribut;nouvelleValeur");
			String inputs [] = scan.nextLine().split(";");
			sysAdmin.modifierCompagnie(inputs[0], inputs[1], inputs[2]);
			printMessage(" (u)Undo - (c)Confirmer");
			input = scan.nextLine();
			if(input.equalsIgnoreCase("u")) {
				sysAdmin.undo();
				gestionCompagnie(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}else if(input.equalsIgnoreCase("c")) {
				gestionCompagnie(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
		}else if(input.equalsIgnoreCase("mp")) {
			gestionVoyage(typeVoyage, typeStation , typeCompagnie , typeTransport);
		}
		
	}
	
	private static void gestionTrajet(String typeVoyage,String typeStation ,String typeCompagnie ,String typeTransport) {
		printMessage("	(add)Ajouter Trajet "+typeVoyage+" - (del) Supprimer Trajet "+typeVoyage+" - (mod) Modifier Trajet "+typeVoyage+" - (mp) Menu Precedent");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("add")) {
			printMessage(" Entrez 'idCompagnie;idTransport;idTrajet;dateDepart dd-MM-yyyy HH:mm;dateArrivee dd-MM-yyyy HH:mm;idStations");
			String inputs [] = scan.nextLine().split(";");
			if(inputs.length<6) {
				printMessage("Les entrees ne sont pas correct! Reessayez!");
				gestionTrajet(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
			sysAdmin.creerTrajet(inputs[0], inputs[1], inputs[2], "vol", inputs[3], inputs[4], inputs[5]);
			printMessage(" (u)Undo - (c)Confirmer");
			input = scan.nextLine();
			if(input.equalsIgnoreCase("u")) {
				sysAdmin.undo();
				gestionTrajet(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}else if(input.equalsIgnoreCase("c")) {
				gestionTrajet(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
			
		}else if(input.equalsIgnoreCase("del")) {
			printMessage(" Entrez 'id"+typeVoyage);
			input = scan.nextLine();
			sysAdmin.supprimerTrajet(input);
			printMessage(" (u)Undo - (c)Confirmer");
			input = scan.nextLine();
			if(input.equalsIgnoreCase("u")) {
				sysAdmin.undo();
				gestionTrajet(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}else if(input.equalsIgnoreCase("c")) {
				gestionTrajet(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
			
		}else if(input.equalsIgnoreCase("mod")) {
			printMessage(" Entrez 'id"+typeVoyage+";attribut;nouvelleValeur");
			String inputs [] = scan.nextLine().split(";");
			sysAdmin.modifierTrajet(inputs[0], inputs[1], inputs[2]);
			printMessage(" (u)Undo - (c)Confirmer");
			input = scan.nextLine();
			if(input.equalsIgnoreCase("u")) {
				sysAdmin.undo();
				gestionTrajet(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}else if(input.equalsIgnoreCase("c")) {
				gestionTrajet(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
			
		}else if(input.equalsIgnoreCase("mp")) {
			gestionVoyage(typeVoyage, typeStation , typeCompagnie , typeTransport);
		}
		
	}
	
	private static void gestionSection(String typeVoyage,String typeStation ,String typeCompagnie ,String typeTransport) {
		printMessage("	(add)Ajouter Section "+typeTransport+" - (mp) Menu Precedent");
		if(input.equalsIgnoreCase("add")) {
			printMessage(" Entrez 'idCompagnie;idTransport;typeSection;disposition;nbPlace");
			String inputs [] = scan.nextLine().split(";");
			if(inputs.length<5) {
				printMessage("Les entrees ne sont pas correct! Reessayez!");
				gestionSection(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
			sysAdmin.ajouterSection(inputs[0], inputs[1], inputs[2], inputs[3], inputs[4]);
			printMessage(" (u)Undo - (c)Confirmer");
			input = scan.nextLine();
			if(input.equalsIgnoreCase("u")) {
				sysAdmin.undo();
				gestionSection(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}else if(input.equalsIgnoreCase("c")) {
				gestionSection(typeVoyage, typeStation , typeCompagnie , typeTransport);
			}
		}else if(input.equalsIgnoreCase("mp")) {
			gestionVoyage(typeVoyage, typeStation , typeCompagnie , typeTransport);
		}
		
	}
	
	private static void consultation(String typeVoyage,String typeStation ,String typeCompagnie ,String typeTransport) {
		printMessage("	(c)Pour consultation par compagnie "+typeCompagnie+" - (s)Pour consultation par "+typeStation+" - (mp) Menu Precedent");
		input = scan.nextLine();
		if(input.equalsIgnoreCase("c")) {
			printMessage("	Entrez id de la compagnie");
			List<String> res = sysAdmin.consultationParCompagnie(scan.nextLine());
			afficher(res);
		}else if(input.equalsIgnoreCase("s")) {
			printMessage("	Entrez id de la station");
			List<String> res = sysAdmin.consultationParStation(scan.nextLine());
			afficher(res);
		}else if(input.equalsIgnoreCase("mp")) {
			gestionVoyage(typeVoyage, typeStation , typeCompagnie , typeTransport);
		}
		
	}

	private static void printMessage(String msg) {
		System.out.println(msg);
	}
	
	private static void afficher(List<String> list){
		if(list != null){
			ListIterator<String> i = list.listIterator();
			while(i.hasNext()){
				System.out.println(i.next());
			}
		}
	}

}
