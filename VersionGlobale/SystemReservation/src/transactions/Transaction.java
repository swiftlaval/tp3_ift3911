package transactions;

public abstract class Transaction {

	private CarteCredit carte;
	private double montant;

	public double getMontant() {
		return this.montant;
	}

	protected Transaction(double montant, CarteCredit carte) {
		this.montant = montant;
		this.carte = carte;
	}

}