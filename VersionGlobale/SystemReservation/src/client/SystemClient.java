package client;

import reservations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import administration.Administrateur;
import database.*;
import database.sections.Affaire;
import database.sections.EcoPremium;
import database.sections.Economie;
import database.sections.Economique;
import database.sections.Famille;
import database.sections.FamilleDeluxe;
import database.sections.Interieure;
import database.sections.Premiere;
import database.sections.PremiereT;
import database.sections.Section;
import database.sections.Suite;
import database.sections.VueSurOcean;
import database.trajets.*;
import patrons.Observateur;
import patrons.Sujet;

public class SystemClient implements Observateur {

	private ControleurReservation controleurReservation;
	private VisiteurClient visiteur;
	private Gestionnaire gestionnaire;
	private String derniereRecherche;
	private List<Client> clients;
	private Sujet sujet;
	private static SystemClient instance;

	@Override
	public void update() {
		String msg = (String) sujet.getUpdate(this);
		if(msg == null){
			System.out.println(":: Pas de nouvelles!");
		}else{
			getClients().forEach(client -> System.out.println(client.getNom()+"! Une messge vaut votre attention!\n:: Message::"+msg));
		}
		
	}
	
	@Override
	public void setSujet(Sujet s) {
		this.sujet = s;
		
	}
	
	public Sujet getSujet() {
		return sujet;
	}

	private SystemClient() {
		this.clients = new ArrayList();
		this.controleurReservation = ControleurReservation.getInstance();
		this.visiteur = new VisiteurClient();
		this.gestionnaire = Gestionnaire.getInstance();
		instance = this;
	}

	public static SystemClient getInstance() {
		if (instance == null)
			instance = new SystemClient();
		return instance;
	}

	/**
	 * 
	 * @param origine
	 * @param dest
	 * @param date
	 * @param section
	 */
	public List<String> verificationTrajet(String origine, String dest, String date, String section) {
		derniereRecherche = origine + ";" + dest + ";" + date + ";" + section;
		List<Trajet> trajets = gestionnaire.recherche(origine, dest, date, section);
		visiteur.setSectionType(section);
		List<String> res = new ArrayList<String>();
		ListIterator<Trajet> i = trajets.listIterator();
		while(i.hasNext()){
			i.next().accept(visiteur);
			res.add(visiteur.getResultat());
		}
		return res;
	}

	/**
	 * 
	 * @param idTrajet
	 * @param priorite
	 * @return
	 */
	public String reserverPlace(String idTrajet, int priorite) {
		Trajet trajet = gestionnaire.trouverTrajet(idTrajet);
		if (trajet == null)
			return null;
		return controleurReservation.effectuerReservation(trajet, priorite);
	}

	/**
	 * 
	 * @param noReservation
	 * @param nomClient
	 * @param adresseClient
	 * @param courriel
	 * @param telephone
	 * @param dateNaissance
	 * @param numeroPasseport
	 * @param dateExpirationPasseport
	 * @param numeroCarteCredit
	 * @param dateExpirationCarte
	 * @return numeroConfirmation ou null si paiement pas effectu�
	 */
	public boolean paiementReservation(String noReservation, String nomClient, String adresseClient, String courriel, 
			String telephone, String dateNaissance, String numeroPasseport, String dateExpirationPasseport, 
			String numeroCarteCredit, String dateExpirationCarte) {
		return controleurReservation.payerReservation(noReservation, nomClient, adresseClient, courriel, telephone, 
				dateNaissance, numeroPasseport, dateExpirationPasseport, numeroCarteCredit, dateExpirationCarte);
	}

	/**
	 * Annuler une reservation
	 * @param noRes
	 * @param noCarteCredit
	 * @param dateExpiration
	 * @return
	 */
	public boolean annulationReservation(String noRes, String noCarteCredit, String dateExpiration) {
		double ratioRemboursement = 0.9;
		return controleurReservation.annulerReservation(noRes, noCarteCredit, dateExpiration, ratioRemboursement);
	}

	/**
	 * Modifier le trajet d'une reservation
	 * @param noReservation
	 * @param idNouveauTrajet
	 * @return
	 */
	public boolean modifierTrajetReservation(String noReservation, String idNouveauTrajet) {
		Trajet nouveauTrajet = gestionnaire.trouverTrajet(idNouveauTrajet);
		if (nouveauTrajet == null)
			return false;
		return controleurReservation.modifierTrajetReservation(noReservation, nouveauTrajet);
	}

	/**
	 * Modifier la section du siege d'une reservation
	 * @param noRes
	 * @param idTrajet
	 * @param nomNouvSection
	 * @param noCarteCredit si reservation confirmee (null sinon)
	 * @param dateExpiration si reservation confirmee (null sinon)
	 * @return
	 */
	public boolean modifierSectionReservation(String noRes, String idTrajet, String nomNouvSection, String noCarteCredit, String dateExpiration) {
		Trajet trajet = gestionnaire.trouverTrajet(idTrajet);
		Section nouvelleSection = null;
		if (trajet.getClass().isInstance(Vol.class))
			nouvelleSection = trouverSectionVol(trajet, nomNouvSection);
		else if (trajet.getClass().isInstance(Croisiere.class))
			nouvelleSection = trouverSectionCroisiere(trajet, nomNouvSection);
		else if (trajet.getClass().isInstance(TrajetTrain.class))
			nouvelleSection = trouverSectionTrain(trajet, nomNouvSection);
		if (nouvelleSection == null)
			return false;
		return controleurReservation.modifierSectionReservation(noRes, nouvelleSection, noCarteCredit, dateExpiration, 0.9);
	}

	private Section trouverSectionVol(Trajet trajet, String classeSection) {
		switch (classeSection.toUpperCase()) {
		case "F":
			return trajet.getSectionsParTrajet().stream().filter(sect -> sect.getClass().isInstance(Premiere.class)).findAny().orElse(null);
		case "A":
			return trajet.getSectionsParTrajet().stream().filter(sect -> sect.getClass().isInstance(Affaire.class)).findAny().orElse(null);
		case "P":
			return trajet.getSectionsParTrajet().stream().filter(sect -> sect.getClass().isInstance(EcoPremium.class)).findAny().orElse(null);
		case "E":
			return trajet.getSectionsParTrajet().stream().filter(sect -> sect.getClass().isInstance(Economique.class)).findAny().orElse(null);
		default:
			return null;
		}
	}

	private Section trouverSectionCroisiere(Trajet trajet, String classeSection) {
		switch (classeSection.toUpperCase()) {
		case "I":
			return trajet.getSectionsParTrajet().stream().filter(sect -> sect.getClass().isInstance(Interieure.class)).findAny().orElse(null);
		case "O":
			return trajet.getSectionsParTrajet().stream().filter(sect -> sect.getClass().isInstance(VueSurOcean.class)).findAny().orElse(null);
		case "S":
			return trajet.getSectionsParTrajet().stream().filter(sect -> sect.getClass().isInstance(Suite.class)).findAny().orElse(null);
		case "F":
			return trajet.getSectionsParTrajet().stream().filter(sect -> sect.getClass().isInstance(Famille.class)).findAny().orElse(null);
		case "D":
			return trajet.getSectionsParTrajet().stream().filter(sect -> sect.getClass().isInstance(FamilleDeluxe.class)).findAny().orElse(null);
		default:
			return null;
		}
	}

	private Section trouverSectionTrain(Trajet trajet, String classeSection) {
		switch (classeSection.toUpperCase()) {
		case "P":
			return trajet.getSectionsParTrajet().stream().filter(sect -> sect.getClass().isInstance(PremiereT.class)).findAny().orElse(null);
		case "E":
			return trajet.getSectionsParTrajet().stream().filter(sect -> sect.getClass().isInstance(Economie.class)).findAny().orElse(null);
		default:
			return null;
		}
	}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

}