package client;

import java.util.*;
import reservations.*;

public class Client {
	public Client(String nom, String adresse, String courriel, String telephone,
			String dateNaissance, String noPasseport, String dateExpirationPasseport) {
		super();
		this.nom = nom;
		this.adresse = adresse;
		this.courriel = courriel;
		this.telephone = telephone;
		this.dateNaissance = dateNaissance;
		this.noPasseport = noPasseport;
		this.dateExpirationPasseport = dateExpirationPasseport;
	}

	private List<Reservation> reservations;
	private String nom;
	private String adresse;
	private String courriel;
	private String telephone;
	private String dateNaissance;
	private String noPasseport;
	private String dateExpirationPasseport;

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCourriel() {
		return courriel;
	}

	public void setCourriel(String courriel) {
		this.courriel = courriel;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getNoPasseport() {
		return noPasseport;
	}

	public void setNoPasseport(String noPasseport) {
		this.noPasseport = noPasseport;
	}

	public String getDateExpirationPasseport() {
		return dateExpirationPasseport;
	}

	public void setDateExpirationPasseport(String dateExpirationPasseport) {
		this.dateExpirationPasseport = dateExpirationPasseport;
	}

	public void update(String nom, String adresse, String telephone, String dateNaissance, String noPasseport, String dateExpirationPasseport) {
		this.nom = nom;
		this.adresse = adresse;
		this.telephone = telephone;
		this.dateNaissance = dateNaissance;
		this.noPasseport = noPasseport;
		this.dateExpirationPasseport = dateExpirationPasseport;
	}

}