package PattrenSingleton;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

public class Singleton {
	private static Singleton firstInstatnce = null;
	
	//TODO - put all factories as an attribute
	
	private Singleton(){
		
	}
	
	public static Singleton getInstance(){
		if(firstInstatnce ==null){
			firstInstatnce = new Singleton();
		}
		
		return firstInstatnce;
	}
	

}
