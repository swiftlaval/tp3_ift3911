package PatronCommande;

import PatronObservateur.Compagnie;

public class CommandeCreerCompagnie implements Commande {

	private Gestionnaire gestionnaire;
	private String idCompagnie;

	public CommandeCreerCompagnie(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
	}
	
	@Override
	public void execute(String... parametres) {
		// TODO Auto-generated method stub
	}

	@Override
	public void undo() {
		gestionnaire.supprimerCompagnie(idCompagnie);
	}

}