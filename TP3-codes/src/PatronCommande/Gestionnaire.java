package PatronCommande;

import java.util.ArrayList;
import java.util.List;

import PatronObservateur.Compagnie;
import PatronObservateur.Trajet;
import trajets.Station;
import trajets.Transport;

public class Gestionnaire {

	private static Gestionnaire instance;
	private List<Compagnie> compagnies;
	private List<Station> stations;
	//private List<Transport> transports;
	private List<Trajet> trajets;
	private List<Transport> transports;

	private Gestionnaire() {
		this.compagnies = new ArrayList<>();
		this.stations = new ArrayList<>();
		//this.transports = new ArrayList<>();
		this.trajets = new ArrayList<>();
		instance = this;
	}
	
	public static Gestionnaire getInstance() {
		if (instance == null)
			return new Gestionnaire();
		return instance;
	}
	
	public String creerCompagnie(String...parametres) {
		// TODO - implement
		return null;
	}
	
	public String creerStation(String...parametres) {
		// TODO - implement
		return null;
	}
	
	public String creerTransport(String...parametres) {
		// TODO - implement
		return null;
	}
	
	public String creerTrajet(String...parametres) {
		// TODO - implement
		return null;
	}
	
	public boolean modifierCompagnie(String id, String attribut, String nouvelleValeur) {
		// TODO - implement
		return false;
	}
	
	public boolean modifierStation(String id, String attribut, String nouvelleValeur) {
		// TODO - implement
		return false;
	}
	
	public boolean modifierTransport(String id, String attribut, String nouvelleValeur) {
		// TODO - implement
		return false;
	}
	
	public boolean modifierTrajet(String id, String attribut, String nouvelleValeur) {
		// TODO - implement
		return false;
	}
	
	public boolean supprimerCompagnie(String id) {
		// TODO - implement
		return false;
	}
	
	public boolean supprimerStation(String id) {
		// TODO - implement
		return false;
	}
	
	public boolean supprimerTransport(String id) {
		// TODO - implement
		return false;
	}
	
	public boolean supprimerTrajet(String id) {
		// TODO - implement
		return false;
	}
}