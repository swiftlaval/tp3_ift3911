package PatronCommande;

public interface Commande {

	/**
	 * 
	 * @param parametres
	 */
	void execute(String... parametres);

	void undo();

}