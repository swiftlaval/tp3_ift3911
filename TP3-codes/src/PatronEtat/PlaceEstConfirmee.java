package PatronEtat;

public class PlaceEstConfirmee implements EtatPlace {
	private Place place;
	
	public PlaceEstConfirmee(Place place){
		this.place = place;
	}

	public void librerPlace() {
		place.setEstDisponible(true);
		place.setEtatPlace(place.getEtatLibre());
		System.out.println("Place est libree");

	}

	public void confirmerPlace(String noReservation) {
		System.out.println("Place est deja confirmee");

	}

	public String reserverPlace() {
		System.out.println("Desole! la place #"+place.getNoPlace()+ " est deja confirmee pour un client avec no de reservation de " + place.getNoReservationDePlace());
		return null;

	}

}
