package PatronEtat;

public class clientPatronEtat {
	public static void main(String[] args){
		Place place = new Place("1");
		System.out.println("Reserver:");
		place.reserverPlace();
		System.out.println("Librer:");
		place.librerPlace();
		System.out.println("Librer:");
		place.librerPlace();
		System.out.println("Confirmer:");
		place.confirmerPlace(place.getNoReservationDePlace());
		System.out.println("Reserver:");
		place.reserverPlace();
		System.out.println("Confirmer:");
		place.confirmerPlace(place.getNoReservationDePlace());
		System.out.println("Confirmer:");
		place.confirmerPlace(place.getNoReservationDePlace());
		System.out.println("Reserver:");
		place.reserverPlace();
	}
	
}
