package PatronEtat;

public class PlaceEstReservee implements EtatPlace {
	private Place place;
	
	public PlaceEstReservee(Place place){
		this.place = place;
	}

	public void librerPlace() {
		place.setEstDisponible(true);
		place.setEtatPlace(place.getEtatLibre());
		System.out.println("Place est libree");

	}

	public void confirmerPlace(String noReservation) {
		place.setEstDisponible(false);
		place.setEtatPlace(place.getEtatConfirme());
		System.out.println("Place est confirmee");

	}

	public String reserverPlace() {
		System.out.println("Desole la place est deja reservee");
		return null;

	}

}
