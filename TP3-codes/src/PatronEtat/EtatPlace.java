package PatronEtat;

public interface EtatPlace {
	void librerPlace();
	void confirmerPlace(String noReservation);
	String reserverPlace();
}
