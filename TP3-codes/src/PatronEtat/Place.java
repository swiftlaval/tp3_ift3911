package PatronEtat;

public class Place {
	private EtatPlace placeEstLibre;
	private EtatPlace placeEstConfirmee;
	private EtatPlace placeEstReservee;
	
	private EtatPlace etatPlace;
	
	private String noPlace;
	private String noReservation;
	boolean estDisponible = true;
	
	public Place(String noPlace){
		this.setNoPlace(noPlace);
		placeEstLibre = new PlaceEstLibre(this);
		placeEstConfirmee = new PlaceEstConfirmee(this);
		placeEstReservee = new PlaceEstReservee(this);
		
		if(estDisponible == true){
			etatPlace = placeEstLibre; 
		}else{

		}
	}
	
	void setEtatPlace(EtatPlace etatPlace){
		this.etatPlace = etatPlace;
	}
	
	EtatPlace getEtatPlace(){
		return etatPlace;
	}
	
	void setEstDisponible(boolean estDisponible){
		this.estDisponible = estDisponible;
	}
	
	public void librerPlace(){
		etatPlace.librerPlace();
	}
	
	public void confirmerPlace(String noReservation){
		etatPlace.confirmerPlace(noReservation);
	}
	
	public void reserverPlace(){
		setNoReservationDePlace(etatPlace.reserverPlace());
	}
	
	public void setNoReservationDePlace(String noReservation){
		this.noReservation = noReservation;
	}
	
	public String getNoReservationDePlace(){
		return this.noReservation;
	}
	
	public EtatPlace getEtatLibre(){return placeEstLibre;}
	public EtatPlace getEtatConfirme(){return placeEstConfirmee;}
	public EtatPlace getEtatReserve(){return placeEstReservee;}

	public String getNoPlace() {
		return noPlace;
	}

	public void setNoPlace(String noPlace) {
		this.noPlace = noPlace;
	}

}
