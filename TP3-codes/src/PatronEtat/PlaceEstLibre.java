package PatronEtat;

import java.util.UUID;

public class PlaceEstLibre implements EtatPlace {
	private Place place;
	
	public PlaceEstLibre(Place place){
		this.place = place;
	}
	

	public void librerPlace() {
		if(place.estDisponible== true){
			System.out.println("Place est deja libre");
		}else{
			place.estDisponible = true;
			System.out.println("Place est libre");
		}
	}

	public void confirmerPlace(String noReservation) {
		System.out.println("Veuilliez d'abord reserver la place!");

	}

	public String reserverPlace() {
		place.setEstDisponible(false);
		place.setEtatPlace(place.getEtatReserve());
		String noReservation = generateString();
		System.out.println("Place est reservee. Votre no de reservation est le:"+ noReservation);
		return noReservation;

	}
	
    public static String generateString() {
        return UUID.randomUUID().toString().substring(0,8);
    }

}
