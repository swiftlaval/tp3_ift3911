package PatronObservateur;

public interface Observateur {
	public void update();
	public void setSujet(Sujet s);
}
