package PatronObservateur;

public interface Sujet {
	
	public void abonner(Observateur o);
	public void desabonner(Observateur o);
	public void notifierObservateurs();
	public Object getUpdate(Observateur o);

}
