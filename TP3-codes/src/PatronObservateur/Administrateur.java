package PatronObservateur;

public class Administrateur {
	private Sujet sujet;
	private String nom;

	public Administrateur(String nom){
		this.nom = nom;
		
	}
	@Override
	public void update() {
		String msg = (String) sujet.getUpdate(this);
		if(msg == null){
			System.out.println(nom + ":: Pas de nouvelles!");
		}else{
			System.out.println(nom + ":: Message::"+msg);
		}

	}

	@Override
	public void setSujet(Sujet s) {
		this.sujet = s;

	}
	
	public String getNom() {
		return nom;
	}

}
