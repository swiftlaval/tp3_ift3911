package PatronObservateur;

import java.util.Date;

public class Trajet{
	private String idTrajet;
	private Compagnie compagnie;
	private Date tempsDepart;
	private Date tempsArrive;
	private double prix;
	private String origin;
	private String destination;
	private Gestionnaire gestionnaire;
	
	public Trajet(Compagnie compagnie, Date tempsDepart, Date tempsArrive, double prix, String origin, String destination, Gestionnaire g){
		this.setCompagnie(compagnie);
		this.tempsDepart = tempsDepart;
		if(tempsDepart.compareTo(tempsArrive) > 0){
			throw new java.lang.Error("Date arriv� doit etre apr�s la date de d�part");
		}
		this.tempsArrive = tempsArrive;		
        this.origin = origin;
        if(origin.equalsIgnoreCase(destination)){
        	throw new java.lang.Error("L'origin et la destination du vol doivent etre differents");
        }
        this.destination = destination;
        this.prix = prix;
		this.gestionnaire = g;
	}

	public Date getTempsDepart() {
		return tempsDepart;
	}

	public void setTempsDepart(Date tempsDepart) {
		gestionnaire.postMessage("le temps de depart est chang� de: " + this.tempsDepart + " �: " + tempsDepart);
		this.tempsDepart = tempsDepart;
	}

	public Date getTempsArrive() {
		return tempsArrive;
	}

	public void setTempsArrive(Date tempsArrive) {
		gestionnaire.postMessage("le temps de arrive est chang� de: " + this.tempsArrive + " �: " + tempsArrive);
		this.tempsArrive = tempsArrive;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		gestionnaire.postMessage("le origin est chang� de: " + this.origin + " �: " + origin);
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		gestionnaire.postMessage("le destinataire est chang� de: " + this.destination + " �: " + destination);
		this.destination = destination;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		gestionnaire.postMessage("le prix est chang� de: " + this.prix + " �: " + prix);
		this.prix = prix;
	}
	
	public Compagnie getCompagnie() {
		return compagnie;
	}

	public void setCompagnie(Compagnie compagnie) {
		this.compagnie = compagnie;
	}

	public String getIdTrajet() {
		return idTrajet;
	}

	public void setIdTrajet(String idTrajet) {
		this.idTrajet = idTrajet;
	}

}
