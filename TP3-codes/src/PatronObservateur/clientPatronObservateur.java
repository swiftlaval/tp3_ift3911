package PatronObservateur;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import PatronObservateur.Gestionnaire;

public class clientPatronObservateur {
	public static void main(String[] args){
		//Les observateurs
		Observateur matiar = new Administrateur("Matiar");
		Observateur lygia = new Administrateur("Lygia");
		Observateur moncef = new Administrateur("Moncef");
		Observateur rosalie = new Administrateur("Rosalie");
		
		//Gestionnaire
		Gestionnaire gestionnaire = Gestionnaire.getInstance();
		gestionnaire.getInstance().abonner(matiar);
		gestionnaire.getInstance().abonner(lygia);
		
		//Compagnie
		Compagnie c = new CompagnieAerienne("Homa", "a1b1c1", gestionnaire);

		
		c.setAddress("a2b2c2");
		gestionnaire.getInstance().desabonner(lygia);
		
		//Trajet
		Date td = null;
		Date ta = null;
		String tempsDepart = "THU Dec 28 20:56:02 EDT 2018";
		String tempsArrive = "THU Dec 29 10:12:02 EDT 2018";
        SimpleDateFormat parser = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
        try {
        	td = parser.parse(tempsDepart);
        	ta = parser.parse(tempsArrive);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Trajet trajet = new Vol((CompagnieAerienne) c, td, ta, 1234.4, "T�h�ran", "Toronto", gestionnaire);
		
		gestionnaire.getInstance().abonner(matiar);
		gestionnaire.getInstance().abonner(lygia);
		gestionnaire.getInstance().abonner(moncef);
		gestionnaire.getInstance().abonner(rosalie);
		
		gestionnaire.getInstance().desabonner(moncef);
		gestionnaire.getInstance().desabonner(lygia);
		trajet.setDestination("Montreal");
		c.setAddress("a3b3c3");
		
		//gestionnaire.modifieCompagnie(c.getIdCompagnie(),c);
		
		//Test DB
		Compagnie compDB = (Compagnie) gestionnaire.compagnieDB.get(c.getIdCompagnie());
		System.out.println("saved address : "  + compDB.getAddress() + " for a "+ compDB.getClass().getName() + " with id " + compDB.getIdCompagnie());
		
		Trajet trajDB = (Trajet) gestionnaire.trajetDB.get(trajet.getIdTrajet());
		System.out.println("saved destionation : "  + trajDB.getDestination() + " for a "+ trajDB.getClass().getName()+ " with id " + trajDB.getIdTrajet());
		
		gestionnaire.suprimeTrajet(trajet.getIdTrajet());
		
		Trajet traj2DB = (Trajet) gestionnaire.trajetDB.get(trajet.getIdTrajet());
		if(traj2DB == null) {
			System.out.println("the target has been deleted");

		}
		
	}

}
