package PatronObservateur;

import PatronObservateur.Gestionnaire;
public class Compagnie {
	private String nom;
	private String address;
	private String idCompagnie;

	private Gestionnaire gestionnaire;

	public Compagnie(String nom, String address, Gestionnaire g){
		this.nom = nom;
		this.address = address;
		this.gestionnaire = g;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		gestionnaire.postMessage("le nom de la compagnie est chang� de: " + this.nom + " �: " + nom);
		this.nom = nom;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		gestionnaire.postMessage("l'addresse de la compagnie est chang� de: " + this.address + " �: " + address);
		this.address = address;
	}

	public String getIdCompagnie() {
		return idCompagnie;
	}
	
	public void setIdCompagnie(String idCompagnie) {
		this.idCompagnie = idCompagnie;
	}

}
