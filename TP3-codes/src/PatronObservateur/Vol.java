package PatronObservateur;

import java.util.Date;
import java.util.UUID;

public class Vol extends Trajet{
	private String idVol;
	
	public Vol(CompagnieAerienne compagnie, Date tempsDepart, Date tempsArrive, double prix, String origin, String destination, Gestionnaire g){
		super(compagnie, tempsDepart, tempsArrive, prix, origin, destination, g);
		this.idVol = compagnie.getNom().toLowerCase().substring(0,2) + UUID.randomUUID().toString().substring(0,5);
		super.setIdTrajet(this.idVol);
		setIdTrajet(idVol);
		g.ajoutTrajet(getIdTrajet(), this);
		
	}
	
	public String getId(){
		return idVol;
	}

}
