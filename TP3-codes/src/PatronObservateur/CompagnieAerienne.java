package PatronObservateur;

import java.util.UUID;

public class CompagnieAerienne extends Compagnie {
	private String idCompagnie;
	
	public CompagnieAerienne(String nom, String address, Gestionnaire g){
		super(nom, address, g);
		this.idCompagnie = nom.toLowerCase().substring(0,3) + UUID.randomUUID().toString().substring(0,3);
		setIdCompagnie(idCompagnie);
		g.ajoutCompagnie(getIdCompagnie(), this);
		
	}

}
