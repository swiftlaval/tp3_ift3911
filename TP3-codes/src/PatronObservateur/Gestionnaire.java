package PatronObservateur;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Gestionnaire implements Sujet {
	private List<Observateur> observateurs;
	private static Gestionnaire uniqueGest;
	private String message;
	private boolean changed;
	
	//DB
	Map<String, Object> compagnieDB = new HashMap<String, Object>();
	Map<String, Object> sectionDB = new HashMap<String, Object>();
	Map<String, Object> transportDB = new HashMap<String, Object>();
	Map<String, Object> stationDB = new HashMap<String, Object>();
	Map<String, Object> placeDB = new HashMap<String, Object>();
	Map<String, Object> trajetDB = new HashMap<String, Object>();
	Map<String, Object> reservationDB = new HashMap<String, Object>();
	
	private Gestionnaire() {
		this.observateurs = new ArrayList<>();
	}
	
	public static synchronized Gestionnaire getInstance() {
		if(uniqueGest == null) {
			uniqueGest = new Gestionnaire();
		}
		return uniqueGest;
	}
	
	public void postMessage(String msg){
		this.message = msg;
		this.changed = true;
		notifierObservateurs();
	}

	@Override
	public void abonner(Observateur o) {
		if(o == null) throw new NullPointerException("Observateur null!");
		if(!observateurs.contains(o)){
			observateurs.add(o);
			o.setSujet(this);
			System.out.println("L'observateur "+((Administrateur) o).getNom()+" est abonn� au " + this.getClass().getName());
		}
	}

	@Override
	public void desabonner(Observateur o) {
		observateurs.remove(o);
		System.out.println("L'observateur "+((Administrateur) o).getNom()+" est desabon� de " + this.getClass().getName());
	}

	@Override
	public void notifierObservateurs() {
		if(!changed)
			return;
		this.changed = false;
		for (Observateur o : this.observateurs)
			o.update();
	}

	@Override
	public Object getUpdate(Observateur o) {
		return this.message;
	}
	
	public void ajoutCompagnie(String id, Compagnie compagnie) {
		postMessage("La compagnie: " + compagnie.getClass().getName() + " " + compagnie.getNom() + " est ajout� avec id: "+ compagnie.getIdCompagnie());
		compagnieDB.put(compagnie.getIdCompagnie(), compagnie);
	}
	
	public void ajoutTrajet(String id, Trajet trajet) {
		postMessage("Le trajet: " + trajet.getClass().getName() + " " + trajet.getOrigin() + "-" + trajet.getDestination() + " est ajout� avec id: "+trajet.getIdTrajet());
		trajetDB.put(trajet.getIdTrajet(), trajet);
	}
	
	public void suprimeCompagnie(String id) {
		Compagnie c = (Compagnie) compagnieDB.get(id);
		postMessage("La compagnie: " + c.getClass().getName() + " " + c.getNom() + " est supprim�e.");
		compagnieDB.remove(c.getIdCompagnie());
	}
	
	public void suprimeTrajet(String id) {
		Trajet t = (Trajet) trajetDB.get(id);
		postMessage("Le trajet: " + t.getClass().getName() + " " + t.getOrigin() + "-" + t.getDestination() + " est supprim�.");
		trajetDB.remove(t.getIdTrajet());
	}
	
	/* inutile!!!
	public boolean modifieCompagnie(String id, Compagnie compagnie) {
		Compagnie c = (Compagnie) compagnieDB.get(id);
		if(c.equals(compagnie)) {
			return false;
		}else {
			compagnieDB.put(id, compagnie);
			postMessage("Attention! l'info de la compagnie: " + compagnie.getNom() + " a �t� chang�");
			return true;
		}
	}
	
	public boolean modifieTrajet(String id, Trajet trajet) {
		Trajet t = (Trajet) trajetDB.get(id);
		if(t.equals(trajet)) {
			return false;
		}else {
			trajetDB.put(id, trajet);
			postMessage("Attention! l'info de la compagnie: " + trajet.getIdTrajet() + " a �t� chang�");
			return true;
		}
	}
	*/

}
