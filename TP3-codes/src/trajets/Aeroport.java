package trajets;

import java.util.UUID;

public class Aeroport extends Station {

	public Aeroport(String nom, String ville) {
		super(nom, ville);
		super.setId(ville.toLowerCase().substring(0,2) + UUID.randomUUID().toString().substring(0,1));
	}

}
