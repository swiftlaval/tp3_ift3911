package trajets;

public class Station {

	private String id;
	private String nom;
	private String ville;
	
	protected Station(String nom, String ville) {
		this.nom =  nom;
		this.ville = ville;
	}

	protected void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getNom() {
		return nom;
	}
	
	public String getVille() {
		return ville;
	}
}
