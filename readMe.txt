﻿Application Système de Gestion de Réservation
L’application fonctionne via la ligne de commande. Une fois elle qu'est exécutée, elle demande d'entrer des lettres pour vous diriger vers le Volet Administratif ou
 bien le Volet Client. 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Menu :
À chaque pas, le menu s’affiche sur écran comme une ligne et demande à l’usager d'entrer des caractères pour accéder à la bonne fonctionnalité. Les ‘hint’s guide 
l’usager par rapport aux entrées plus spécifiques par exemple « nom usager » et le « mot de passe » pour accéder au Volet Administratif. 
À tout moment l’usager est capable de retourner au menu précèdent en entrant « mp ».
Exemple : 
« Veuilliez entrez 'a' pour acceder au système admin ou 'c' pour le système client »
a
« SYSTEME ADMIN: 
 username;password  hint: Matiar;1111 - Lygia;2222 - Moncef;3333 - Rosalie;4444 - (mp)Menu Precedant »
Matiar ;1111
« Bienvenu Matiar! Veuilliez choissir parmi les options suivantes: (a)Avion - (p)Paquebot - (t)Train - (mp)Menu Precedant »
a
« Systeme de gestion de Vol:
 (s)Gestion Aeroport (c)Gestion Compagnie Aerienne - (t)Gestion Vol - (g)Gestion Section de Avion - (x)Consultation - (mp)Menu Precedant »
mp
« SYSTEME ADMIN: 
 username;password  hint: Matiar;1111 - Lygia;2222 - Moncef;3333 - Rosalie;4444 - (mp)Menu Precedant »
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Message :
Certains messages sont ajoutés afin de montrer la fonctionnalité des patrons. Par exemple en ajoutant une Station, les messages envoyés aux Administrateur abonnés 
affiches sur l’écran (Patron Observateur). Evidement ces messages sont seulement pour montrer que le patron fonctionne et ils simulent le cas où le message est reçu 
par les autres observateurs.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Fonctionnalités :
Les usagers Administrateur et Client sont capable de faire leurs différentes fonctionnalités tel que demander par l’énoncé pour les trois types de voyages. 

